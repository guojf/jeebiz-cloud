package com.jeefw.cloud.httpws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableAutoConfiguration
@SpringBootApplication
//@EnableDiscoveryClient
public class HttpwsApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(HttpwsApplication.class, args);
	}
	
}

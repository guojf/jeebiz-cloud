package com.jeefw.cloud.httpws;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.jeefw.cloud.httpws.ws.jaxrs.Product;
import com.jeefw.cloud.httpws.ws.jaxrs.ProductService;

public class JAXRSClient1 {


    public static void main(String[] args) {  
        String baseAddress = "http://localhost:8082/services/ws/rest";  
  
        List<Object> providerList = new ArrayList<Object>();  
        providerList.add(new JacksonJsonProvider());  
  
        ProductService productService = JAXRSClientFactory.create(baseAddress, ProductService.class, providerList);  
        List<Product> productList = productService.retrieveAllProducts();  
        for (Product product : productList) {  
            System.out.println(product);  
        }  
    }  
    
}

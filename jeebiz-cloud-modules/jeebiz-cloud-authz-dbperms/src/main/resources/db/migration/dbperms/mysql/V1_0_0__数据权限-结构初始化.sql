
/* 数据权限表：数据规则表、数据规则组表、数据规则-数据规则组关系表、角色-数据规则关系表、角色-数据规则组关系表、用户-数据规则关系表、用户-数据规则组关系表*/

-- Create table
create table SYS_AUTHZ_SCOPE_GROUP (
  GROUP_ID    VARCHAR2(32) default sys_guid() not null,
  GROUP_NAME  VARCHAR2(60),
  PRIMARY KEY (GROUP_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_SCOPE_GROUP  is '数据规则组表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_SCOPE_GROUP.GROUP_ID  is '数据规则组表ID';
comment on column SYS_AUTHZ_SCOPE_GROUP.GROUP_NAME  is '规则组名称';

-- Create table
create table SYS_AUTHZ_SCOPE_RULE (
  RULE_ID       VARCHAR2(32) default sys_guid() not null,
  RULE_TYPE     VARCHAR2(2) not null,
  RULE_SUBJECT  VARCHAR2(50) not null,
  RULE_COLUMN   VARCHAR2(100) not null,
  PRIMARY KEY (RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_SCOPE_RULE  is '数据规则表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_SCOPE_RULE.RULE_ID  is '数据规则表ID';
comment on column SYS_AUTHZ_SCOPE_RULE.RULE_TYPE  is '数据规则类型(1:原生|2:继承|3:复制)';
comment on column SYS_AUTHZ_SCOPE_RULE.RULE_SUBJECT  is '数据规则约束主体(表或视图名称)';
comment on column SYS_AUTHZ_SCOPE_RULE.RULE_COLUMN  is '属性或者表字段';

-- Create table
create table SYS_AUTHZ_SCOPE_GROUP_REL (
  GROUP_ID    VARCHAR2(20) not null,
  RULE_ID     VARCHAR2(32) not null,
  OP          VARCHAR2(20),
  PRIMARY KEY (GROUP_ID,RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_SCOPE_GROUP_REL  is '数据规则-数据规则组关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_SCOPE_GROUP_REL.GROUP_ID  is '数据规则组ID';
comment on column SYS_AUTHZ_SCOPE_GROUP_REL.RULE_ID  is '数据规则ID';
comment on column SYS_AUTHZ_SCOPE_GROUP_REL.OP  is '运算关系(and|or)';

-- Create table
create table SYS_AUTHZ_ROLE_SCOPE_REL (
  APP_ID      VARCHAR2(32) not null,
  ROLE_ID     VARCHAR2(32) not null,
  RULE_ID     VARCHAR2(32) not null,
  PRIMARY KEY (ROLE_ID,RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_ROLE_SCOPE_REL  is '角色-数据规则关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_ROLE_SCOPE_REL.APP_ID  is '应用ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_REL.ROLE_ID  is '角色ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_REL.RULE_ID  is '数据规则ID';

-- Create table
create table SYS_AUTHZ_ROLE_SCOPE_GROUP_REL (
  APP_ID      VARCHAR2(32) not null,
  ROLE_ID     VARCHAR2(32) not null,
  GROUP_ID    VARCHAR2(32) not null,
  PRIMARY KEY (GROUP_ID,ROLE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_ROLE_SCOPE_GROUP_REL  is '角色-数据规则组关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_ROLE_SCOPE_GROUP_REL.APP_ID  is '应用ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_GROUP_REL.ROLE_ID  is '角色ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_GROUP_REL.GROUP_ID  is '数据规则组ID';

-- Create table
create table SYS_AUTHZ_USER_SCOPE_REL (
  APP_ID      VARCHAR2(32) not null,
  USER_ID     VARCHAR2(32) not null,
  RULE_ID     VARCHAR2(32) not null,
  PRIMARY KEY (USER_ID, RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_USER_SCOPE_REL  is '用户-数据规则关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_USER_SCOPE_REL.APP_ID  is '应用ID';
comment on column SYS_AUTHZ_USER_SCOPE_REL.USER_ID  is '用户ID';
comment on column SYS_AUTHZ_USER_SCOPE_REL.RULE_ID  is '数据规则ID';

-- Create table
create table SYS_AUTHZ_USER_SCOPE_GROUP_REL (
  APP_ID      VARCHAR2(32) not null,
  USER_ID     VARCHAR2(32) not null,
  GROUP_ID    VARCHAR2(32) not null,
  PRIMARY KEY (GROUP_ID,USER_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_USER_SCOPE_GROUP_REL  is '用户-数据规则组关系表';
-- Add comments to the columns
comment on column SYS_AUTHZ_USER_SCOPE_GROUP_REL.APP_ID  is '应用ID'; 
comment on column SYS_AUTHZ_USER_SCOPE_GROUP_REL.USER_ID  is '用户ID';
comment on column SYS_AUTHZ_USER_SCOPE_GROUP_REL.GROUP_ID  is '数据规则组ID';

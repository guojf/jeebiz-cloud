
/* 数据权限表：数据规则表、数据规则组表、数据规则-数据规则组关系表、角色-数据规则关系表、角色-数据规则组关系表、用户-数据规则关系表、用户-数据规则组关系表*/

-- Create table
create table SYS_AUTHZ_SCOPE_GROUP (
  G_ID    		VARCHAR2(32) default sys_guid() not null,
  G_NAME  		VARCHAR2(60),
  G_STATUS		VARCHAR2(2) default 1,
  G_TIME24		VARCHAR2(32) default to_char(sysdate ,'yyyy-mm-dd hh24:mi:ss'),
  CONSTRAINT PK_SCOPE_GROUP PRIMARY KEY(G_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_SCOPE_GROUP  is '数据规则组表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_SCOPE_GROUP.G_ID  is '数据规则组表ID';
comment on column SYS_AUTHZ_SCOPE_GROUP.G_NAME  is '规则组名称';
comment on column SYS_AUTHZ_SCOPE_GROUP.G_STATUS  is '规则组状态（0:禁用|1:可用）';
comment on column SYS_AUTHZ_SCOPE_GROUP.G_TIME24  is '初始化时间';

-- Create table
create table SYS_AUTHZ_SCOPE_RULE (
  R_ID       	VARCHAR2(32) default sys_guid() not null,
  R_TYPE     	VARCHAR2(2) not null,
  R_SUBJECT  	VARCHAR2(50) not null,
  R_COLUMN   	VARCHAR2(100) not null,
  R_STATUS		VARCHAR2(2) default 1,
  R_TIME24		VARCHAR2(32) default to_char(sysdate ,'yyyy-mm-dd hh24:mi:ss'),
  CONSTRAINT PK_SCOPE_RULE PRIMARY KEY(R_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_SCOPE_RULE  is '数据规则表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_SCOPE_RULE.R_ID  is '数据规则表ID';
comment on column SYS_AUTHZ_SCOPE_RULE.R_TYPE  is '数据规则类型(1:原生|2:继承|3:复制)';
comment on column SYS_AUTHZ_SCOPE_RULE.R_SUBJECT  is '数据规则约束主体(表或视图名称)';
comment on column SYS_AUTHZ_SCOPE_RULE.R_COLUMN  is '属性或者表字段';
comment on column SYS_AUTHZ_SCOPE_RULE.R_STATUS  is '规则状态（0:禁用|1:可用）';
comment on column SYS_AUTHZ_SCOPE_RULE.R_TIME24  is '初始化时间';

-- Create table
create table SYS_AUTHZ_SCOPE_GROUP_REL (
  G_ID    		VARCHAR2(20) not null,
  R_ID     		VARCHAR2(32) not null,
  OP          	VARCHAR2(20),
  PRIMARY KEY (GROUP_ID,RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_SCOPE_GROUP_REL  is '数据规则-数据规则组关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_SCOPE_GROUP_REL.G_ID  is '数据规则组ID';
comment on column SYS_AUTHZ_SCOPE_GROUP_REL.R_ID  is '数据规则ID';
comment on column SYS_AUTHZ_SCOPE_GROUP_REL.OP  is '运算关系(and|or)';

-- Create table
create table SYS_AUTHZ_ROLE_SCOPE_REL (
  APP_ID      VARCHAR2(32) not null,
  ROLE_ID     VARCHAR2(32) not null,
  RULE_ID     VARCHAR2(32) not null,
  PRIMARY KEY (ROLE_ID,RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_ROLE_SCOPE_REL  is '角色-数据规则关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_ROLE_SCOPE_REL.APP_ID  is '应用ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_REL.ROLE_ID  is '角色ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_REL.RULE_ID  is '数据规则ID';

-- Create table
create table SYS_AUTHZ_ROLE_SCOPE_GROUP_REL (
  APP_ID      VARCHAR2(32) not null,
  ROLE_ID     VARCHAR2(32) not null,
  GROUP_ID    VARCHAR2(32) not null,
  PRIMARY KEY (GROUP_ID,ROLE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_ROLE_SCOPE_GROUP_REL  is '角色-数据规则组关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_ROLE_SCOPE_GROUP_REL.APP_ID  is '应用ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_GROUP_REL.ROLE_ID  is '角色ID';
comment on column SYS_AUTHZ_ROLE_SCOPE_GROUP_REL.GROUP_ID  is '数据规则组ID';

-- Create table
create table SYS_AUTHZ_USER_SCOPE_REL (
  APP_ID      VARCHAR2(32) not null,
  USER_ID     VARCHAR2(32) not null,
  RULE_ID     VARCHAR2(32) not null,
  PRIMARY KEY (USER_ID, RULE_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_USER_SCOPE_REL  is '用户-数据规则关系表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_USER_SCOPE_REL.APP_ID  is '应用ID';
comment on column SYS_AUTHZ_USER_SCOPE_REL.USER_ID  is '用户ID';
comment on column SYS_AUTHZ_USER_SCOPE_REL.RULE_ID  is '数据规则ID';

-- Create table
create table SYS_AUTHZ_USER_SCOPE_GROUP_REL (
  APP_ID      VARCHAR2(32) not null,
  USER_ID     VARCHAR2(32) not null,
  GROUP_ID    VARCHAR2(32) not null,
  PRIMARY KEY (GROUP_ID,USER_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_USER_SCOPE_GROUP_REL  is '用户-数据规则组关系表';
-- Add comments to the columns
comment on column SYS_AUTHZ_USER_SCOPE_GROUP_REL.APP_ID  is '应用ID'; 
comment on column SYS_AUTHZ_USER_SCOPE_GROUP_REL.USER_ID  is '用户ID';
comment on column SYS_AUTHZ_USER_SCOPE_GROUP_REL.GROUP_ID  is '数据规则组ID';

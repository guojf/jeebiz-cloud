package com.jeefw.cloud.httpdoc.service;

import java.util.List;

import com.jeefw.cloud.httpdoc.dao.entities.Httpdoc;

public interface HttpdocIndexService {

	Httpdoc get(Integer id);

	List<Httpdoc> list();
	
}

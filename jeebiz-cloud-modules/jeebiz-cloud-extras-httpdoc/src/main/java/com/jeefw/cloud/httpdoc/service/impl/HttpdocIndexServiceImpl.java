package com.jeefw.cloud.httpdoc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jeefw.cloud.httpdoc.dao.daointerface.HttpdocIndexDao;
import com.jeefw.cloud.httpdoc.dao.entities.Httpdoc;
import com.jeefw.cloud.httpdoc.service.HttpdocIndexService;

@Service("httpdocIndexService")
public class HttpdocIndexServiceImpl implements HttpdocIndexService {

	@Autowired
	protected HttpdocIndexDao httpdocIndexDao;

	@Override
	public Httpdoc get(Integer id) {
		return getHttpdocIndexDao().get(id);
	}

	@Override
	public List<Httpdoc> list() {
		return getHttpdocIndexDao().list();
	}

	public HttpdocIndexDao getHttpdocIndexDao() {
		return httpdocIndexDao;
	}

	public void setHttpdocIndexDao(HttpdocIndexDao httpdocIndexDao) {
		this.httpdocIndexDao = httpdocIndexDao;
	}
	
}

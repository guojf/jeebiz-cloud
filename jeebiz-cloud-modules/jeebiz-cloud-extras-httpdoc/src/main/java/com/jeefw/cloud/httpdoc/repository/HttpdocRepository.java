package com.jeefw.cloud.httpdoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jeefw.cloud.httpdoc.dao.entities.Httpdoc;

@Repository
public interface HttpdocRepository extends JpaRepository<Httpdoc, Long> {

}

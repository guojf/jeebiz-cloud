package com.jeefw.cloud.httpdoc.dao.daointerface;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jeefw.cloud.httpdoc.dao.entities.Httpdoc;

@Repository("httpdocIndexDao")
public interface HttpdocIndexDao {

	Httpdoc get(Integer id);

	List<Httpdoc> list();

}

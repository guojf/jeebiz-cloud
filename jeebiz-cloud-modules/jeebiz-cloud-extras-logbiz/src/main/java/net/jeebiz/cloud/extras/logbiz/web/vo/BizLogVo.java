/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.web.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 业务操作日志信息Vo
 */
@ApiModel(value = "BizLogVo", description = "业务操作日志信息Vo")
@Getter
@Setter
@ToString
public class BizLogVo {

	/**
	 * 日志ID
	 */
	@ApiModelProperty(name = "id", dataType = "String", value = "日志记录ID")
	private String id;
	/**
	 * 功能模块
	 */
	@ApiModelProperty(name = "module", dataType = "String", value = "功能模块名称")
	private String module;
	/**
	 * 业务名称
	 */
	@ApiModelProperty(name = "business", dataType = "String", value = "业务名称")
	private String business;
	/**
	 * 操作类型
	 */
	@ApiModelProperty(name = "opt", dataType = "String", value = "操作类型")
	private String opt;
	/**
	 * 日志级别：（debug:调试、info:信息、warn:警告、error:错误、fetal:严重错误）
	 */
	@ApiModelProperty(name = "level", dataType = "String", value = "日志级别：（debug:调试、info:信息、warn:警告、error:错误、fetal:严重错误）")
	private String level;
	/**
	 * 功能操作请求来源IP
	 */
	@ApiModelProperty(name = "addr", dataType = "String", value = " 功能操作请求来源IP")
	private String addr;
	/**
	 * 功能操作请求来源IP地址所在地
	 */
	@ApiModelProperty(name = "location", dataType = "String", value = "功能操作请求来源IP地址所在地")
	private String location;
	/**
	 * 操作描述
	 */
	@ApiModelProperty(name = "msg", dataType = "String", value = "操作描述")
	private String msg;
	/**
	 * 功能操作异常信息
	 */
	@ApiModelProperty(name = "exception", dataType = "String", value = "功能操作异常信息")
	private String exception;
	/**
	 * 操作人ID
	 */
	@ApiModelProperty(name = "userId", dataType = "String", value = "操作人ID")
	private String userId;
	/**
	 * 操作人名称
	 */
	@ApiModelProperty(name = "userName", dataType = "String", value = "操作人名称")
	private String userName;
	/**
	 * 操作发生时间
	 */
	@ApiModelProperty(name = "time24", dataType = "String", value = "操作发生时间")
	private String time24;

}

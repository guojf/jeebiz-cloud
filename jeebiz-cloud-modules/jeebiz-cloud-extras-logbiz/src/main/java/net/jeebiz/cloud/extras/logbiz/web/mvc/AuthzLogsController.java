/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.web.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.api.webmvc.Result;
import net.jeebiz.cloud.extras.logbiz.dao.entities.AuthzLogModel;
import net.jeebiz.cloud.extras.logbiz.service.IAuthzLogService;
import net.jeebiz.cloud.extras.logbiz.utils.enums.AuthzOptEnum;
import net.jeebiz.cloud.extras.logbiz.utils.enums.LoggerLevelEnum;
import net.jeebiz.cloud.extras.logbiz.web.vo.AuthzLogPaginationVo;
import net.jeebiz.cloud.extras.logbiz.web.vo.AuthzLogVo;

@Api(tags = "认证授权日志 : （用户登录、登出日志信息）")
@RestController
@RequestMapping("/extras/logs/authz/")
public class AuthzLogsController extends BaseApiController {

	@Autowired
	private IAuthzLogService authzLogService;
	
	@ApiOperation(value = "认证授权类型", notes = "认证授权类型（login:登录认证、logout:会话注销）")
	@GetMapping("opts")
	@PreAuthorize("authenticated")
	@ResponseBody
	public Object authzOpts() throws Exception {
		return AuthzOptEnum.toList();
	}

	@ApiOperation(value = "日志级别", notes = "日志级别（debug:调试、info:信息、warn:警告、error:错误、fetal:严重错误）")
	@GetMapping("levels")
	@PreAuthorize("authenticated")
	@ResponseBody
	public Object levels() throws Exception {
		return LoggerLevelEnum.toList();
	}
	
	@ApiOperation(value = "认证授权日志", notes = "分页查询用户登录、登出日志信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "paginationVo", value = "数据筛选条件", dataType = "AuthzLogPaginationVo")
	})
	@PostMapping("list")
	@PreAuthorize("authenticated and (hasAuthority('logs-authz:list') or hasAuthority('*')) ")
	@ResponseBody
	public Object list(@Valid @RequestBody AuthzLogPaginationVo paginationVo)
			throws Exception {
		
		AuthzLogModel model = getBeanMapper().map(paginationVo, AuthzLogModel.class);
		Page<AuthzLogModel> pageResult = getAuthzLogService().getPagedList(model);
		List<AuthzLogVo> retList = new ArrayList<AuthzLogVo>();
		for (AuthzLogModel logModel : pageResult.getRecords()) {
			retList.add(getBeanMapper().map(logModel, AuthzLogVo.class));
		}
		
		return new Result<AuthzLogVo>(pageResult, retList);
	}

	public IAuthzLogService getAuthzLogService() {
		return authzLogService;
	}

	public void setAuthzLogService(IAuthzLogService authzLogService) {
		this.authzLogService = authzLogService;
	}
	
}

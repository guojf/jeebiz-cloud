/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.service;

import net.jeebiz.cloud.api.service.IBaseService;
import net.jeebiz.cloud.extras.logbiz.dao.entities.AuthzLogModel;

/**
 * 认证授权日志Service
 */
public interface IAuthzLogService extends IBaseService<AuthzLogModel>{

	
}

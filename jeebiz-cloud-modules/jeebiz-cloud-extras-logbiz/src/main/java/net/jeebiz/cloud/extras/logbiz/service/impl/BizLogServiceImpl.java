/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.extras.logbiz.dao.IBizLogDao;
import net.jeebiz.cloud.extras.logbiz.dao.entities.BizLogModel;
import net.jeebiz.cloud.extras.logbiz.service.IBizLogService;

/**
 * 业务操作日志Service实现
 */
@Service
public class BizLogServiceImpl extends BaseServiceImpl<BizLogModel, IBizLogDao>
		implements IBizLogService {

}

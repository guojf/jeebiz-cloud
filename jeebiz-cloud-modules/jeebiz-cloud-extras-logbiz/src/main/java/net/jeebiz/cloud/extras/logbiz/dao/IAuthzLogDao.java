/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.dao;

import org.apache.ibatis.annotations.Mapper;

import net.jeebiz.cloud.api.dao.BaseDao;
import net.jeebiz.cloud.extras.logbiz.dao.entities.AuthzLogModel;

/**
 * 
 */		
@Mapper
public interface IAuthzLogDao extends BaseDao<AuthzLogModel>{
	
	
}

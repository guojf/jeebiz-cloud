/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.extras.logbiz.dao.IAuthzLogDao;
import net.jeebiz.cloud.extras.logbiz.dao.entities.AuthzLogModel;
import net.jeebiz.cloud.extras.logbiz.service.IAuthzLogService;

/**
 * 认证授权日志Service实现
 */
@Service
public class AuthzLogServiceImpl extends BaseServiceImpl<AuthzLogModel, IAuthzLogDao>
		implements IAuthzLogService {
 
}

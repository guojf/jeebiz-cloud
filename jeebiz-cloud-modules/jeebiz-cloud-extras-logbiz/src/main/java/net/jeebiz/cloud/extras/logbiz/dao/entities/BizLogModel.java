/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.logbiz.dao.entities;

import org.apache.ibatis.type.Alias;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.dao.entities.PaginationModel;

/**
 * 功能操作日志信息表Model
 */
@Alias("BizLogModel")
@SuppressWarnings("serial")
@Getter
@Setter
@ToString
public class BizLogModel extends PaginationModel<BizLogModel> {

	/**
	 * 操作对象角色
	 */
	private String roleId;
	/**
	 * 日志记录ID编号
	 */
	private String id;
	/**
	 * 功能模块
	 */
	private String module;
	/**
	 * 业务名称
	 */
	private String business;
	/**
	 * 操作类型
	 */
	private String opt;
	/**
	 * 日志级别：（debug:调试、info:信息、warn:警告、error:错误、fetal:严重错误）
	 */
	private String level;
	/**
	 * 功能操作请求来源IP地址
	 */
	private String addr;
	/**
	 * 功能操作请求来源IP地址所在地
	 */
	private String location;
	/**
	 * 功能操作描述
	 */
	private String msg;
	/**
	 * 功能操作异常信息
	 */
	private String exception;
	/**
	 * 功能操作人ID
	 */
	private String userId;
	/**
	 * 操作人名称
	 */
	private String userName;
	/**
	 * 功能操作发生时间
	 */
	private String time24;
	/**
	 * 功能操作发生起始时间
	 */
	private String begintime;
	/**
	 * 功能操作发生结束时间
	 */
	private String endtime;
	/**
	 * 关键词搜索
	 */
	private String keywords;

}

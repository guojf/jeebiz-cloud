package net.jeebiz.cloud.extras.quartz.service.impl;

import java.util.List;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.extras.quartz.dao.IQuartzJobDao;
import net.jeebiz.cloud.extras.quartz.dao.entities.QuartzJobModel;
import net.jeebiz.cloud.extras.quartz.service.IQuartzJobService;
import net.jeebiz.cloud.extras.quartz.setup.JobStatus;
import net.jeebiz.cloud.extras.quartz.setup.quartz.QuartzManager;

@Service
public class QuartzJobServiceImpl extends BaseServiceImpl<QuartzJobModel, IQuartzJobDao> implements IQuartzJobService {

	public final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private QuartzManager quartzManager;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void initJob() {
		// 	这里获取任务信息数据
		List<QuartzJobModel> jobList = getDao().getJobList();
		for (QuartzJobModel jobModel : jobList) {
			 if (JobStatus.RUNNING.getCode().equals(jobModel.getStatus())) {
				 getQuartzManager().addJob(jobModel);
			 }
		}
	}

	@Override
	public QuartzJobModel getQuartzJobByBiz(String bizId) {
		return getDao().getJobByBizId(bizId);
	}
	
	/**
	 * 添加任务
	 * @param scheduleJob
	 * @throws SchedulerException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int addJob(QuartzJobModel job) {
		getQuartzManager().addJob(job);
		return getDao().insert(job);
	}

	/**
	 * 暂停一个job
	 * 
	 * @param job
	 * @throws SchedulerException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int pauseJob(QuartzJobModel job) throws SchedulerException {
		getQuartzManager().pauseJob(job);
		// 任务执行状态（0:待执行|1:运行中|2:暂停中|3:被锁定|4:已完成|5:失败|6:删除）
		return getDao().setStatus(job.getId(), "2");
	}

	/**
	 * 恢复一个job
	 * 
	 * @param job
	 * @throws SchedulerException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int resumeJob(QuartzJobModel job) throws SchedulerException {
		getQuartzManager().resumeJob(job);
		// 任务执行状态（0:待执行|1:运行中|2:暂停中|3:被锁定|4:已完成|5:失败|6:删除）
		return getDao().setStatus(job.getId(), "0");
	}

	/**
	 * 删除一个job
	 * 
	 * @param job
	 * @throws SchedulerException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int deleteJob(QuartzJobModel job) throws SchedulerException {
		getQuartzManager().deleteJob(job);
		// 任务执行状态（0:待执行|1:运行中|2:暂停中|3:被锁定|4:已完成|5:失败|6:删除）
		return getDao().setStatus(job.getId(), "6");
	}

	/**
	 * 立即执行job
	 * 
	 * @param job
	 * @throws SchedulerException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int runJobNow(QuartzJobModel job) throws SchedulerException {
		getQuartzManager().runJobNow(job);
		return 1;
	}

	/**
	 * 更新job时间表达式
	 * 
	 * @param job
	 * @throws SchedulerException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int updateJob(QuartzJobModel job) throws SchedulerException {
		getQuartzManager().updateJobCron(job);
		return getDao().update(job);
	}

	public QuartzManager getQuartzManager() {
		return quartzManager;
	}

}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.quartz.setup;

public class Constants {
	
	public static final String EXTRAS_JOB = "Extras-Job";

	public static class Biz {
		
		public static final String JOB_LIST = "任务执行列表";
		public static final String JOB_DETAIL = "任务详细";
		public static final String JOB_PRE_PAUSE = "任务执行前的发布任务暂停";
		public static final String JOB_PRE_RESUME = "任务执行前的发布任务恢复";
		
	}
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.quartz.setup.config;

import org.flywaydb.spring.boot.ext.FlywayFluentConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzJobConfiguration {
	
	@Bean
	public FlywayFluentConfiguration quartzJobConfiguration() {
		
		FlywayFluentConfiguration configuration = new FlywayFluentConfiguration("quartz",
				"定时任务-模块初始化", "1.0.0");
		
		return configuration;
	}
	
}

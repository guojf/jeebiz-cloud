/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.quartz.setup.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import net.jeebiz.cloud.extras.quartz.service.IQuartzJobService;

@Component
@Order(value = 1)
public class QuartzJobInitListener implements CommandLineRunner {

    @Autowired
    private IQuartzJobService quartzJobService;

    @Override
    public void run(String... arg0) throws Exception {
        try {
        	getQuartzJobService().initJob();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public IQuartzJobService getQuartzJobService() {
		return quartzJobService;
	}

}
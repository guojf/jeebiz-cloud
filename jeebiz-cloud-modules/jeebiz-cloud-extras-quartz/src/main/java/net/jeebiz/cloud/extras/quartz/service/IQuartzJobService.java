package net.jeebiz.cloud.extras.quartz.service;

import org.quartz.SchedulerException;

import net.jeebiz.cloud.api.service.IBaseService;
import net.jeebiz.cloud.extras.quartz.dao.entities.QuartzJobModel;

public interface IQuartzJobService extends IBaseService<QuartzJobModel>{

	void initJob();

	QuartzJobModel getQuartzJobByBiz(String bizId);
	
	int addJob(QuartzJobModel task);
	
	int pauseJob(QuartzJobModel task) throws SchedulerException;
	
	int resumeJob(QuartzJobModel task) throws SchedulerException;
	
	int deleteJob(QuartzJobModel task) throws SchedulerException;
	
	int runJobNow(QuartzJobModel task) throws SchedulerException;
	
	int updateJob(QuartzJobModel task) throws SchedulerException;
	
}

package net.jeebiz.cloud.extras.quartz.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.jeebiz.cloud.api.dao.BaseDao;
import net.jeebiz.cloud.extras.quartz.dao.entities.QuartzJobModel;


@Mapper
public interface IQuartzJobDao extends BaseDao<QuartzJobModel>{

	QuartzJobModel getJobByBizId(@Param("id") String bizId);

	List<QuartzJobModel> getJobList();
	
}
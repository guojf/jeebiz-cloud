/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.setup;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(FaceRecognitionProperties.PREFIX)
public class FaceRecognitionProperties {

	public static final String PREFIX = "face";

	/**
	 * 人脸识别服务提供者（local: 本地实现, baidu: 百度人脸识别服务）
	 */
	private String provider = "local";
	/**
	 * 人脸识别数据分组（以学校代码为分组）
	 */
	private String group = "14535";
	
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

}

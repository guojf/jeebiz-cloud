/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.web.vo;

import org.hibernate.validator.constraints.SafeHtml;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.vo.AbstractPaginationVo;

@ApiModel(value = "AuthzFaceRepositoryPaginationVo", description = "人脸识别数据分组分页查询参数")
@Getter
@Setter
@ToString
public class AuthzFaceRepositoryPaginationVo extends AbstractPaginationVo {

	/**
	 * 人脸识别数据分组
	 */
	@ApiModelProperty(name = "name", dataType = "String", value = "分组名称")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String name;
	/**
	 * 人脸识别数据分组状态(0:不可用|1:正常)
	 */
	@ApiModelProperty(name = "status", dataType = "String", value = "分组状态：0:不可用|1:正常", allowableValues = "1,0")
	private String status;
	
}

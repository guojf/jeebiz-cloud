/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.service;

import net.jeebiz.cloud.api.service.IBaseService;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceRepositoryModel;

public interface IAuthzFaceRepositoryService extends IBaseService<AuthzFaceRepositoryModel>{

}

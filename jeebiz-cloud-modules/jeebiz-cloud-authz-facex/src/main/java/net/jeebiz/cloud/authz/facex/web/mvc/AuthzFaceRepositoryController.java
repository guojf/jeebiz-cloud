/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.web.mvc;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.annotation.BusinessLog;
import net.jeebiz.cloud.api.annotation.BusinessType;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.api.webmvc.Result;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceRepositoryModel;
import net.jeebiz.cloud.authz.facex.service.IAuthzFaceRepositoryService;
import net.jeebiz.cloud.authz.facex.setup.Constants;
import net.jeebiz.cloud.authz.facex.web.vo.AuthzFaceRepositoryNewVo;
import net.jeebiz.cloud.authz.facex.web.vo.AuthzFaceRepositoryPaginationVo;
import net.jeebiz.cloud.authz.facex.web.vo.AuthzFaceRepositoryRenewVo;
import net.jeebiz.cloud.authz.facex.web.vo.AuthzFaceVo;

@Api(tags = "人脸识别：存储接口")
@RestController
@RequestMapping(value = "/authz/face/repo/")
public class AuthzFaceRepositoryController extends BaseApiController {

	@Autowired
	protected IAuthzFaceRepositoryService authzFaceRepositoryService;
	
	@ApiOperation(value = "分页查询人脸识别数据分组信息", notes = "分页查询人脸识别数据分组信息")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "body", name = "paginationVo", value = "分页查询参数", dataType = "AuthzFaceRepositoryPaginationVo") 
	})
	@BusinessLog(module = Constants.AUTHZ_FACE_REPO, business = "分页查询机构信息", opt = BusinessType.SELECT)
	@PostMapping("list")
	@PreAuthorize("authenticated and (hasAuthority('face-repo:list') or hasAuthority('*')) ")
	@ResponseBody
	public Object list(@Valid @RequestBody AuthzFaceRepositoryPaginationVo paginationVo) throws Exception {
		
		AuthzFaceRepositoryModel model = getBeanMapper().map(paginationVo, AuthzFaceRepositoryModel.class);
		Page<AuthzFaceRepositoryModel> pageResult = getAuthzFaceRepositoryService().getPagedList(model);
		List<AuthzFaceVo> retList = Lists.newArrayList();
		for (AuthzFaceRepositoryModel orgModel : pageResult.getRecords()) {
			retList.add(getBeanMapper().map(orgModel, AuthzFaceVo.class));
		}
		
		return new Result<AuthzFaceVo>(pageResult, retList);
		
	}
	
	@ApiOperation(value = "增加人脸识别数据分组信息", notes = "增加人脸识别数据分组信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "repoVo", value = "人脸识别数据分组信息", dataType = "AuthzFaceRepositoryNewVo")
	})
	@BusinessLog(module = Constants.AUTHZ_FACE_REPO, business = Constants.Biz.AUTHZ_FACE_REPO_NEW, opt = BusinessType.INSERT)
	@PostMapping("new")
	@PreAuthorize("authenticated and (hasAuthority('face-repo:new') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<String> faceRepo(@Valid @RequestBody AuthzFaceRepositoryNewVo repoVo) throws Exception { 
		int total = getAuthzFaceRepositoryService().getCountByName(repoVo.getName(), null);
		if(total > 0) {
			return fail("face.repo.new.name-exists");
		}
		AuthzFaceRepositoryModel model = getBeanMapper().map(repoVo, AuthzFaceRepositoryModel.class);
		int ret = getAuthzFaceRepositoryService().insert(model);
		if(ret > 0) {
			return success("face.repo.new.success", ret);
		}
		return fail("face.repo.new.fail", ret);
	}
	
	@ApiOperation(value = "修改人脸识别数据分组信息", notes = "修改人脸识别数据分组信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "repoVo", value = "人脸识别数据分组信息", dataType = "AuthzFaceRepositoryRenewVo")
	})
	@BusinessLog(module = Constants.AUTHZ_FACE_REPO, business = Constants.Biz.AUTHZ_FACE_REPO_RENEW, opt = BusinessType.UPDATE)
	@PostMapping("renew")
	@PreAuthorize("authenticated and (hasAuthority('face-repo:renew') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<String> renew(@Valid @RequestBody AuthzFaceRepositoryRenewVo repoVo) throws Exception { 
		int total = getAuthzFaceRepositoryService().getCountByName(repoVo.getName(), repoVo.getId());
		if(total > 0) {
			return fail("face.repo.new.name-exists");
		}
		AuthzFaceRepositoryModel model = getBeanMapper().map(repoVo, AuthzFaceRepositoryModel.class);
		int ret = getAuthzFaceRepositoryService().update(model);
		if(ret > 0) {
			return success("face.repo.renew.success", ret);
		}
		return fail("face.repo.renew.fail", ret);
	}
	
	@ApiOperation(value = "删除人脸识别数据分组信息", notes = "删除人脸识别数据分组信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam( name = "id", required = true, value = "人脸识别数据分组ID", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_FACE_REPO, business = Constants.Biz.AUTHZ_FACE_REPO_DELETE, opt = BusinessType.DELETE)
	@PostMapping("delete/{id}")
	@PreAuthorize("authenticated and (hasAuthority('face-repo:delete') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<String> delFace(@Valid @NotNull(message = "人脸识别数据分组ID不能为空") @RequestParam String id) throws Exception {
		int count = getAuthzFaceRepositoryService().getCountByUid(id);
		if(count > 0) {
			return fail("face.repo.delete.child-exists");
		}
		int total = getAuthzFaceRepositoryService().delete(id);
		if(total > 0) {
			return success("face.repo.delete.success", total);
		}
		return success("face.repo.delete.fail", total);
	}

	public IAuthzFaceRepositoryService getAuthzFaceRepositoryService() {
		return authzFaceRepositoryService;
	}
	
}

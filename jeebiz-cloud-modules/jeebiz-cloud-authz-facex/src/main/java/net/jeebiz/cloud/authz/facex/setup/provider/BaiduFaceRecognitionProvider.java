/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.setup.provider;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.baidu.ai.aip.spring.boot.FaceLiveness;
import com.baidu.ai.aip.spring.boot.FaceOption;
import com.baidu.ai.aip.spring.boot.FaceRecognitionV3Template;
import com.baidu.ai.aip.spring.boot.FaceType;
import com.baidu.aip.util.Base64Util;

import net.jeebiz.cloud.authz.facex.dao.IAuthzFaceDao;
import net.jeebiz.cloud.authz.facex.dao.IAuthzFaceRepositoryDao;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceModel;
import net.jeebiz.cloud.authz.facex.setup.FaceRecognitionProperties;

@Component
public class BaiduFaceRecognitionProvider implements FaceRecognitionProvider {

	@Autowired
	private FaceRecognitionV3Template faceRecognitionTemplate;
	@Autowired
	private FaceRecognitionProperties faceRecognitionProperties;
	@Autowired
	private IAuthzFaceDao authzFace;
	@Autowired
	private IAuthzFaceRepositoryDao authzFaceRepository;
	
	@Override
	public String getName() {
		return "baidu";
	}

	@Override
	public JSONObject detect(byte[] imageBytes, String filename) throws Exception {
		JSONObject detect = getFaceRecognitionTemplate().detect(imageBytes, FaceType.LIVE, FaceLiveness.NORMAL);
		if(StringUtils.equalsIgnoreCase(detect.getString("error_code"), "0") && detect.containsKey("result")) {
			JSONObject result = detect.getJSONObject("result");
			result.put("error_code", 0);
			return result;
		}
		return detect;
	}

	@Override
	public JSONObject verify(MultipartFile image) throws Exception {
		// 对文件进行转码
		String imageBase64 = Base64Util.encode(image.getBytes());
		JSONObject verify = getFaceRecognitionTemplate().faceVerify(imageBase64, FaceOption.COMMON);
		if(StringUtils.equalsIgnoreCase(verify.getString("error_code"), "0") && verify.containsKey("result")) {
			JSONObject result = verify.getJSONObject("result");
			result.put("error_code", 0);
			return result;
		}
		return verify;
	}

	@Override
	public JSONObject match(String userId, MultipartFile image) throws Exception {
		AuthzFaceModel model = getAuthzFace().getModel(userId);
		String imageBase64_2 = Base64Util.encode(image.getBytes());
		JSONObject match = getFaceRecognitionTemplate().match(model.getFace(), imageBase64_2);
		if(StringUtils.equalsIgnoreCase(match.getString("error_code"), "0") && match.containsKey("result")) {
			JSONObject result = match.getJSONObject("result");
			result.put("error_code", 0);
			return result;
		}
		return match;
	}

	@Override
	public JSONObject search(MultipartFile image) throws Exception {
		// 对文件进行转码
		String imageBase64 = Base64Util.encode(image.getBytes());
		JSONObject search = getFaceRecognitionTemplate().search(imageBase64, faceRecognitionProperties.getGroup());
		if(StringUtils.equalsIgnoreCase(search.getString("error_code"), "0") && search.containsKey("result")) {
			JSONObject result = search.getJSONObject("result");
			result.put("error_code", 0);
			return result;
		}
		return search;
	}

	@Override
	public JSONObject merge(MultipartFile template, MultipartFile target) throws Exception {
		JSONObject merge = getFaceRecognitionTemplate().merge(template.getBytes(), target.getBytes());
		if(StringUtils.equalsIgnoreCase(merge.getString("error_code"), "0") && merge.containsKey("result")) {
			JSONObject result = merge.getJSONObject("result");
			result.put("error_code", 0);
			return result;
		}
		return merge;
	}
	
	public FaceRecognitionV3Template getFaceRecognitionTemplate() {
		return faceRecognitionTemplate;
	}

	public IAuthzFaceDao getAuthzFace() {
		return authzFace;
	}

	public IAuthzFaceRepositoryDao getAuthzFaceRepository() {
		return authzFaceRepository;
	}

}

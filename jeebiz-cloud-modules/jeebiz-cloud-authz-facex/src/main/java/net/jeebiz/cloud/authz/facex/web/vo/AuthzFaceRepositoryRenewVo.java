/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.web.vo;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.SafeHtml;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ApiModel(value = "AuthzFaceRepositoryRenewVo", description = "人脸识别数据分组更新Vo")
@Accessors(chain = true)
@Getter
@Setter
@ToString
public class AuthzFaceRepositoryRenewVo {

	@ApiModelProperty(name = "id", required = true, dataType = "String", value = "分组ID")
	@NotBlank(message = "分组ID必填")
	private String id;
	
	@ApiModelProperty(name = "name", required = true, dataType = "String", value = "分组名称")
	@NotBlank(message = "分组名称必填")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String name;
	 
}

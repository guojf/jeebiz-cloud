/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.dao;

import org.apache.ibatis.annotations.Mapper;

import net.jeebiz.cloud.api.dao.BaseDao;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceRepositoryModel;

@Mapper
public interface IAuthzFaceRepositoryDao extends BaseDao<AuthzFaceRepositoryModel>{

}

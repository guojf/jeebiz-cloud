/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.service.impl;

import java.util.Base64;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baidu.ai.aip.spring.boot.FaceType;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.facex.dao.IAuthzFaceDao;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceModel;
import net.jeebiz.cloud.authz.facex.service.IAuthzFaceService;
import net.jeebiz.cloud.authz.facex.setup.FaceRecognitionProperties;
import net.jeebiz.cloud.authz.facex.setup.provider.FaceRecognitionProvider;

@Service
public class AuthzFaceServiceImpl extends BaseServiceImpl<AuthzFaceModel, IAuthzFaceDao> implements IAuthzFaceService{

	@Autowired
	private List<FaceRecognitionProvider> faceRecognitionProviders;
	@Autowired
	private FaceRecognitionProperties faceRecognitionProperties;
	
	@Override
	public JSONObject scanning(String userId, MultipartFile image) throws Exception {
		for (FaceRecognitionProvider provider : getFaceRecognitionProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFaceRecognitionProperties().getProvider())) {
				byte[] imageBytes = image.getBytes();
				String imageBase64 = Base64.getEncoder().encodeToString(imageBytes);
				JSONObject detect = provider.detect(imageBytes, image.getOriginalFilename());
				if(StringUtils.equalsIgnoreCase(detect.getString("error_code"), "0")) {
					
					//int  face_num = result.getIntValue("face_num");
					JSONArray face_list = detect.getJSONArray("face_list");
					JSONObject face = face_list.getJSONObject(0);
					
					String face_token = face.getString("face_token");
					
					AuthzFaceModel model = new  AuthzFaceModel();
					
					model.setRid("11");
					model.setId(userId);
					model.setType(FaceType.LIVE.name());
					model.setFace(imageBase64);
					model.setToken(face_token);
					
					// 判断是否已经保存过面部识别结果
					int ct = getDao().getCountByUid(userId);
					if(ct > 0 ) {
						getDao().update(model);
					} else {
						getDao().insert(model);
					}
					
				}
				return detect;
			}
		}
		return null;
	}
	
	@Override
	public JSONObject detect(MultipartFile image) throws Exception {
		for (FaceRecognitionProvider provider : getFaceRecognitionProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFaceRecognitionProperties().getProvider())) {
				return provider.detect(image.getBytes(), image.getOriginalFilename());
			}
		}
		return null;
	}

	@Override
	public JSONObject match(String userId, MultipartFile image) throws Exception {
		for (FaceRecognitionProvider provider : getFaceRecognitionProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFaceRecognitionProperties().getProvider())) {
				return provider.match(userId, image);
			}
		}
		return null;
	}
	
	@Override
	public JSONObject search(MultipartFile image) throws Exception {
		for (FaceRecognitionProvider provider : getFaceRecognitionProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFaceRecognitionProperties().getProvider())) {
				return provider.search(image);
			}
		}
		return null;
	}

	@Override
	public JSONObject merge(MultipartFile template, MultipartFile target) throws Exception {
		for (FaceRecognitionProvider provider : getFaceRecognitionProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFaceRecognitionProperties().getProvider())) {
				return provider.merge(template, target);
			}
		}
		return null;
	}

	public List<FaceRecognitionProvider> getFaceRecognitionProviders() {
		return faceRecognitionProviders;
	}

	public FaceRecognitionProperties getFaceRecognitionProperties() {
		return faceRecognitionProperties;
	}
	
}

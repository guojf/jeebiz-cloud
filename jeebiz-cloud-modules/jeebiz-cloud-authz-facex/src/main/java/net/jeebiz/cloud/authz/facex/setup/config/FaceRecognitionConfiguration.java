/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.setup.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import net.jeebiz.cloud.authz.facex.setup.FaceRecognitionProperties;

@Configuration
@EnableConfigurationProperties(FaceRecognitionProperties.class)
public class FaceRecognitionConfiguration {

}
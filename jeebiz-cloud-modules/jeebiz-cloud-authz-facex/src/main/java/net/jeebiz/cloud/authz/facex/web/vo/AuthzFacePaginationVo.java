/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.web.vo;

import org.hibernate.validator.constraints.SafeHtml;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.vo.AbstractPaginationVo;

@ApiModel(value = "AuthzFacePaginationVo", description = "人脸识别数据分页查询参数")
@Getter
@Setter
@ToString
public class AuthzFacePaginationVo extends AbstractPaginationVo {

	/**
	 * 人脸识别数据分组
	 */
	@ApiModelProperty(name = "name", dataType = "String", value = "人脸识别数据分组")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String name;
	/**
	 * 人脸的类型：(LIVE:表示生活照;通常为手机、相机拍摄的人像图片、或从网络获取的人像图片等|IDCARD:表示身份证芯片照;二代身份证内置芯片中的人像照片|WATERMARK:表示带水印证件照;一般为带水印的小图，如公安网小图|CERT:表示证件照片;如拍摄的身份证、工卡、护照、学生证等证件图片);
	 */
	@ApiModelProperty(name = "type", dataType = "String", value = "人脸识别数据分组", allowableValues = ",LIVE,IDCARD,WATERMARK,CERT")
	private String type;
	
}

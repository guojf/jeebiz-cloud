/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.setup.provider;

import java.util.Base64;

import org.bytedeco.opencv.spring.boot.OpenCVFaceRecognitionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;

import net.jeebiz.cloud.api.exception.BizRuntimeException;
import net.jeebiz.cloud.authz.facex.dao.IAuthzFaceDao;
import net.jeebiz.cloud.authz.facex.dao.IAuthzFaceRepositoryDao;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceModel;

@Component
public class OpenCVFaceRecognitionProvider implements FaceRecognitionProvider {

	@Autowired
	private OpenCVFaceRecognitionTemplate faceRecognitionTemplate;
	@Autowired
	private IAuthzFaceDao authzFace;
	@Autowired
	private IAuthzFaceRepositoryDao authzFaceRepository;
	
	@Override
	public String getName() {
		return "opencv";
	}

	@Override
	public JSONObject detect(byte[] imageBytes, String filename) throws Exception {
    	return getFaceRecognitionTemplate().detect(imageBytes, filename);
	}

	@Override
	public JSONObject verify(MultipartFile image) throws Exception {
		throw new BizRuntimeException("not support");
	}

	@Override
	public JSONObject match(String userId, MultipartFile image) throws Exception {
		AuthzFaceModel model = getAuthzFace().getModel(userId);
		return getFaceRecognitionTemplate().match(Base64.getDecoder().decode(model.getFace()), image.getBytes(), image.getOriginalFilename());
	}

	@Override
	public JSONObject search(MultipartFile image) throws Exception {
		throw new BizRuntimeException("not support");
	}

	@Override
	public JSONObject merge(MultipartFile template, MultipartFile target) throws Exception {
		throw new BizRuntimeException("not support");
	}
	
	public OpenCVFaceRecognitionTemplate getFaceRecognitionTemplate() {
		return faceRecognitionTemplate;
	}

	public IAuthzFaceDao getAuthzFace() {
		return authzFace;
	}

	public IAuthzFaceRepositoryDao getAuthzFaceRepository() {
		return authzFaceRepository;
	}

}

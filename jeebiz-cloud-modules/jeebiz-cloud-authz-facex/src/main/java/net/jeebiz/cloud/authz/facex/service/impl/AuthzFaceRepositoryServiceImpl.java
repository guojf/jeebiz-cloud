/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.facex.dao.IAuthzFaceRepositoryDao;
import net.jeebiz.cloud.authz.facex.dao.entities.AuthzFaceRepositoryModel;
import net.jeebiz.cloud.authz.facex.service.IAuthzFaceRepositoryService;

@Service
public class AuthzFaceRepositoryServiceImpl extends BaseServiceImpl<AuthzFaceRepositoryModel, IAuthzFaceRepositoryDao> implements IAuthzFaceRepositoryService{
	
}

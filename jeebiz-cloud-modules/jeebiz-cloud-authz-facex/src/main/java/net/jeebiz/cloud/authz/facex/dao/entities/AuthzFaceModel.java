/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.dao.entities;

import org.apache.ibatis.type.Alias;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import net.jeebiz.cloud.api.dao.entities.PaginationModel;

@Alias("AuthzFaceModel")
@Accessors(chain = true)
@Getter
@Setter
@SuppressWarnings("serial")
@ToString
@EqualsAndHashCode(callSuper = true)
public class AuthzFaceModel extends PaginationModel<AuthzFaceModel> {
	
	/**
    * 用户ID
    */
	private String id;
	  /**
     * 人脸识别数据分组ID
     */
 	private String rid;
 	/**
	 * 分组名称
	 */
	private String rname;
    /**
     * 人脸识别数据ID
     */
 	private String fid;
    /**
     * 人脸识别图片base64编码后的图片数据（图片的base64编码不包含图片头的，如data:image/jpg;base64,）
     */
	private String face;
	/**
     * 人脸的类型：(LIVE:表示生活照;通常为手机、相机拍摄的人像图片、或从网络获取的人像图片等|IDCARD:表示身份证芯片照;二代身份证内置芯片中的人像照片|WATERMARK:表示带水印证件照;一般为带水印的小图，如公安网小图|CERT:表示证件照片;如拍摄的身份证、工卡、护照、学生证等证件图片); 默认LIVE
     */
	private String type;
	/**
     * 人脸图片的唯一标识
     */
	private String token;
	
}
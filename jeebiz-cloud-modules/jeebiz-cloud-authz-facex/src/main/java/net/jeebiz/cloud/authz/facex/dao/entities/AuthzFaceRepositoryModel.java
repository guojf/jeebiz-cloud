/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.facex.dao.entities;

import org.apache.ibatis.type.Alias;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import net.jeebiz.cloud.api.dao.entities.PaginationModel;

@Alias("AuthzFaceRepositoryModel")
@Accessors(chain = true)
@Getter
@Setter
@SuppressWarnings("serial")
@ToString
@EqualsAndHashCode(callSuper = true)
public class AuthzFaceRepositoryModel extends PaginationModel<AuthzFaceRepositoryModel> {

	/**
	 * 分组ID
	 */
	private String id;
	/**
	 * 分组名称
	 */
	private String name;
	/**
	 * 分组状态（0:禁用|1:可用）
	 */
	private String status;
	/**
	 * 该分组下已有多少人脸识别数据
	 */
	private int faces;
	/**
	 * 初始化时间
	 */
	private String time24;
	
	
}

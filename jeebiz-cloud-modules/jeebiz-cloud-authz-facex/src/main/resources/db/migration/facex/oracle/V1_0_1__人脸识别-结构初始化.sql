
-- Create table
create table SYS_AUTHZ_USER_FACE_REPO (
  R_ID   			VARCHAR2(32) default sys_guid() not null,
  R_NAME   			VARCHAR2(50) not null,
  R_STATUS			VARCHAR2(2) default 1,
  R_TIME24			VARCHAR2(32) default to_char(sysdate ,'yyyy-mm-dd hh24:mi:ss'),
  CONSTRAINT PK_USER_FACE_REPO_UID PRIMARY KEY(R_ID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_USER_FACE_REPO  is '用户-人脸识别数据分组表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_USER_FACE_REPO.R_ID  is '分组ID';
comment on column SYS_AUTHZ_USER_FACE_REPO.R_NAME  is '分组名称';
comment on column SYS_AUTHZ_USER_FACE_REPO.R_STATUS  is '分组状态（0:禁用|1:可用）';
comment on column SYS_AUTHZ_USER_FACE_REPO.R_TIME24  is '初始化时间';

-- Create table
create table SYS_AUTHZ_USER_FACE (
  U_ID   			VARCHAR2(32) not null,
  U_RID   			VARCHAR2(32) not null,
  U_FID   			VARCHAR2(32) default sys_guid() not null,
  U_FACE			CLOB not null,
  U_FACE_TYPE  		VARCHAR2(20) default 'LIVE' not null,
  U_FACE_TOKEN		VARCHAR2(500),
  CONSTRAINT PK_USER_FACE_UID PRIMARY KEY(U_FID)
);
-- Add comments to the table 
comment on table SYS_AUTHZ_USER_FACE  is '用户-人脸识别数据表';
-- Add comments to the columns 
comment on column SYS_AUTHZ_USER_FACE.U_ID  is '用户ID';
comment on column SYS_AUTHZ_USER_FACE.U_RID  is '人脸识别数据分组ID';
comment on column SYS_AUTHZ_USER_FACE.U_FID  is '人脸识别数据ID';
comment on column SYS_AUTHZ_USER_FACE.U_FACE  is '人脸识别图片base64编码后的图片数据（图片的base64编码不包含图片头的，如data:image/jpg;base64,）';
comment on column SYS_AUTHZ_USER_FACE.U_FACE_TYPE  is '人脸的类型：(LIVE:表示生活照;通常为手机、相机拍摄的人像图片、或从网络获取的人像图片等|IDCARD:表示身份证芯片照;二代身份证内置芯片中的人像照片|WATERMARK:表示带水印证件照;一般为带水印的小图，如公安网小图|CERT:表示证件照片;如拍摄的身份证、工卡、护照、学生证等证件图片); 默认LIVE';
comment on column SYS_AUTHZ_USER_FACE.U_FACE_TOKEN  is '人脸图片的唯一标识';	 
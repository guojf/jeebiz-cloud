package com.jeefw.cloud.httpuid.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jeefw.cloud.httpuid.uid.Sequence;

@RestController
@RequestMapping("/http/uid/")
public class HttpuidController {
	
	protected Sequence sequence = new Sequence();
	
	@GetMapping("next")
	public String uid() {
		return String.valueOf(this.sequence.nextId());
	}
	
}

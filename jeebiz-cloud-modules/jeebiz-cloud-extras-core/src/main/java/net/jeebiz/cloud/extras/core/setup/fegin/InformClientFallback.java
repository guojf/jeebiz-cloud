/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.fegin;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import feign.hystrix.FallbackFactory;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.extras.core.web.vo.InformSendVo;

/**
 * 服务降级
 */
@Component
public class InformClientFallback implements FallbackFactory<InformClientApi> {

    protected final Logger logger = LoggerFactory.getLogger(InformClientFallback.class);
    
    @Autowired
    protected FeginFallbackHandler fallbackHandler;

    @Override
    public InformClientApi create(Throwable throwable) {
    	
        fallbackHandler.handle(throwable);

        return new InformClientApi() {

            @Override
            public Object detail(String id) {
                return ApiRestResponse.fail("Feign调度任务异常 /extras/inform/detail/{id} 请求降级，参数 id："+id);
            }

            @Override
            public Object send(@Valid InformSendVo informVo) {
                return ApiRestResponse.fail("Feign调度任务异常 /extras/inform/send 请求降级，参数 InformSendVo："+ JSONObject.toJSONString(informVo));
            }
            
        };
    }
}

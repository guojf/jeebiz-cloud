/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.fegin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class FeginFallbackHandler {
	
	protected final Logger logger = LoggerFactory.getLogger(FeginFallbackHandler.class);

	public void handle(Throwable throwable) {
		logger.info("fallback:", throwable);
	}

}

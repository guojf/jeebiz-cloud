/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import feign.Request;
import feign.Retryer;

@Configuration
public class FeignClientsConfiguration {

	@Value("${feign.request.connectTimeoutMillis:2000}")
    int connectTimeoutMillis;
    @Value("${feign.request.readTimeoutMillis:180000}")
    int readTimeoutMillis;

    @Bean
    @Scope("prototype")
    public Request.Options feginOption() {
        // 读取与连接超时，有一个超时了都会降级
        Request.Options option = new Request.Options(connectTimeoutMillis, readTimeoutMillis);
        return option;
    }
    
    /**
     * 打印请求日志
     * @return
     */
    @Bean
    public feign.Logger.Level multipartLoggerLevel() {
        return feign.Logger.Level.FULL;
    }
    
    @Bean
    public Retryer feignRetryer() {
    	// return new Retryer.Default(period, maxPeriod, maxAttempts)
        return new Retryer.Default();
    }
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.github.vindell.jwt.time.JwtTimeProvider;
import com.github.vindell.jwt.token.SignedWithSecretKeyJWTRepository;

@Configuration
public class WebSecurityConfig {
	
    @Bean
   	public SignedWithSecretKeyJWTRepository backendJwtRepository(JwtTimeProvider timeProvider) {
    	SignedWithSecretKeyJWTRepository jWTRepository = new SignedWithSecretKeyJWTRepository();
    	jWTRepository.setTimeProvider(timeProvider);
   		return jWTRepository;
   	}

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(6);
    }
    
}

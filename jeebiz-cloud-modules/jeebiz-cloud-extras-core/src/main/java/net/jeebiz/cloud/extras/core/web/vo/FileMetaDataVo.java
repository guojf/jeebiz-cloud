package net.jeebiz.cloud.extras.core.web.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 文件元数据(MetaData)
 */
@Getter
@Setter
@ToString
public class FileMetaDataVo {

	private String name;

	private String value;

}

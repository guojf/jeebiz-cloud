/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.aspect;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.spring.boot.utils.Log4jUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.biz.utils.WebUtils;
import org.springframework.security.boot.biz.userdetails.SecurityPrincipal;
import org.springframework.security.boot.utils.SubjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.github.vindell.ip2region.spring.boot.IP2regionTemplate;

import net.jeebiz.cloud.api.annotation.BusinessLog;
import net.jeebiz.cloud.api.annotation.BusinessType;
import net.jeebiz.cloud.api.utils.Constants;


@Aspect
@Component
public class BusinessLog4j2Aspect {
	
	@Autowired
	private IP2regionTemplate ip2regionTemplate;
	
    /** 
     * 定义拦截规则：拦截有@RequestMapping注解的方法。 
     */  
    public void controllerAspect(BusinessLog bizLog){}  
    
    /*<aop:config>
		<aop:pointcut id="logPointcut"  expression="execution(* net.jeebiz..controller.*.*(..)) and @annotation(net.jeebiz.api.annotation.BusinessLog) and @annotation(businessLog)" />
		<aop:aspect id="logAspect" ref="businessLog4j2Aspect">
			<aop:after-returning method="afterReturing"  pointcut-ref="logPointcut" />
		</aop:aspect>
	</aop:config>*/

    
	/**
	 * 代理对象正常调用返回后advice
	 * @param jp
	 * @throws IOException 
	 * @throws TemplateException 
	 */
    @AfterReturning(pointcut = "execution(* net.jeebiz..mvc..**..*.*(..)) and @annotation(net.jeebiz.cloud.api.annotation.BusinessLog) and @annotation(bizLog)")
	public void afterReturing(JoinPoint jp, BusinessLog bizLog) throws IOException{
		
    	Signature signature = jp.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;
		
		String[] argNames = methodSignature.getParameterNames();
		Object[] args = jp.getArgs();
		Map<String,Object> data = new HashMap<String,Object>();
		
		for (int i = 0 , j = argNames.length ; i < j ; i++){
			data.put(argNames[i], args[i]);
		}
		
		ThreadContext.put("module", bizLog.module());
		ThreadContext.put("biz", bizLog.business());
		ThreadContext.put("opt", bizLog.opt().getKey());
		
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		ThreadContext.put("userId", principal.getUserid());
		RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
		ThreadContext.put("location", ip2regionTemplate.getRegion(WebUtils.getRemoteAddr(request)));
		
		// 记录请求日志
		Log4jUtils.instance("net.jeebiz.biz").error(Constants.bizMarker, "业务操作成功.");
		
	}
    
    @AfterThrowing(throwing="ex", pointcut = "execution(* net.jeebiz..mvc..**..*.*(..)) and @annotation(net.jeebiz.cloud.api.annotation.BusinessLog)  and @annotation(bizLog)")
	public void afterThrowing(Throwable ex, BusinessLog bizLog) throws IOException{
    	
    	ThreadContext.put("module", bizLog.module());
		ThreadContext.put("biz", bizLog.business());
		ThreadContext.put("opt", bizLog.opt().getKey());
		// 登录报错,这里还拿不到SecurityPrincipal对象
		if(bizLog.opt().equals(BusinessType.LOGIN)) {
			//ServletRequest request = SubjectUtils.getWebSubject().getServletRequest();
			//ThreadContext.put("userId", request.getParameter("username"));
		} else {
			SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
			ThreadContext.put("userId", principal.getUserid());
		}
		
		ThreadContext.put("clazz", ex.getClass().getName());
		//两个方法在没有使用JSF的项目中是没有区别的
		RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
		ThreadContext.put("location", ip2regionTemplate.getRegion(WebUtils.getRemoteAddr(request)));
		
		// 记录请求日志
		Log4jUtils.instance("net.jeebiz.biz").error(Constants.bizMarker, "业务操作异常：" + ex.getMessage(), ex);
		
		
	}

}



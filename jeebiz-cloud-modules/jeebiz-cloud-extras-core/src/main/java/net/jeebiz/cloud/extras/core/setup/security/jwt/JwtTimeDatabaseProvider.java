/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.security.jwt;

import java.text.ParseException;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.vindell.jwt.time.JwtTimeProvider;

import net.jeebiz.cloud.extras.core.dao.ICommonDao;

@Component
public class JwtTimeDatabaseProvider implements JwtTimeProvider {

	@Autowired
	private ICommonDao commonDao;
	
	@Override
	public long now() {
		try {
			String nowString = getCommonDao().getNow();
			return DateUtils.parseDate(nowString, "yyyy-MM-dd HH:mm:ss").getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return System.currentTimeMillis();
	}

	public ICommonDao getCommonDao() {
		return commonDao;
	}

}

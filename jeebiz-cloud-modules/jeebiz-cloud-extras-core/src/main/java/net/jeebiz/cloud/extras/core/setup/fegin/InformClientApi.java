/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.core.setup.fegin;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import net.jeebiz.cloud.extras.core.setup.config.FeignClientsConfiguration;
import net.jeebiz.cloud.extras.core.web.vo.InformSendVo;


/**
 * 消息服务接口
 */
@FeignClient(name = "devops-inform", fallbackFactory = InformClientFallback.class,configuration = FeignClientsConfiguration.class )
@Component
public interface InformClientApi {

    @GetMapping(value = "/extras/inform/detail/{id}")
    public Object detail(@PathVariable("id") String id);

    @PostMapping(value = "/extras/inform/send")
    public Object send(@Valid @RequestBody InformSendVo informVo);

}


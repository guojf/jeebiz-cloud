package net.jeebiz.cloud.extras.core.setup.config;

import org.springframework.context.annotation.Configuration;

import com.github.vindell.ip2region.spring.boot.EnableIP2region;

@Configuration
@EnableIP2region
public class IP2regionConfiguration {

	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.filestore.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.extras.core.web.vo.FilestoreVo;
import net.jeebiz.cloud.extras.filestore.dao.IFilestoreDao;
import net.jeebiz.cloud.extras.filestore.dao.entities.FilestoreModel;
import net.jeebiz.cloud.extras.filestore.service.IFilestoreService;
import net.jeebiz.cloud.extras.filestore.setup.FilestoreProperties;
import net.jeebiz.cloud.extras.filestore.setup.provider.FilestoreProvider;
import net.jeebiz.cloud.extras.filestore.web.vo.FilestoreDownloadVo;

@Service
public class FilestoreServiceImpl extends BaseServiceImpl<FilestoreModel, IFilestoreDao> implements IFilestoreService{
	
	@Autowired
	private List<FilestoreProvider> filestoreProviders;
	@Autowired
	private FilestoreProperties filestoreProperties;
	
	@Override
	public FilestoreVo upload(MultipartFile file) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.upload(file);
			}
		}
		return null;
	}
	
	@Override
	public List<FilestoreVo> upload(MultipartFile[] files) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.upload(files);
			}
		}
		return Lists.newArrayList();
	}

	@Override
	public boolean deleteByPath(List<String> paths) throws Exception {
		for (FilestoreProvider provider : filestoreProviders) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.deleteByPath(paths);
			}
		}
		return false;
	}
	
	@Override
	public boolean deleteByUuid(List<String> uuids) throws Exception {
		for (FilestoreProvider provider : filestoreProviders) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.deleteByUuid(uuids);
			}
		}
		return false;
	}

	@Override
	public FilestoreVo reupload(String uuid, MultipartFile file) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.reupload(uuid, file);
			}
		}
		return null;
	}
	
	@Override
	public List<FilestoreVo> listByPath(List<String> paths) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.listByPath(paths);
			}
		}
		return null;
	}

	@Override
	public List<FilestoreVo> listByUuid(List<String> uuids) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.listByUuid(uuids);
			}
		}
		return null;
	}
	
	@Override
	public FilestoreDownloadVo downloadByPath(String path) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.downloadByPath(path);
			}
		}
		return null;
	}

	@Override
	public FilestoreDownloadVo downloadByUuid(String uuid) throws Exception {
		for (FilestoreProvider provider : getFilestoreProviders()) {
			if(StringUtils.equalsIgnoreCase(provider.getName(), getFilestoreProperties().getStorage())) {
				return provider.downloadByUuid(uuid);
			}
		}
		return null;
	}

	public List<FilestoreProvider> getFilestoreProviders() {
		return filestoreProviders;
	}

	public FilestoreProperties getFilestoreProperties() {
		return filestoreProperties;
	}
	 
}

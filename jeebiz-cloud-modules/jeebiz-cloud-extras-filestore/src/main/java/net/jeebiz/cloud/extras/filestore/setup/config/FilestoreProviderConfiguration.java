/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.filestore.setup.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.github.tobato.fastdfs.spring.boot.FastdfsTemplate;

import io.minio.MinioClient;
import net.jeebiz.cloud.extras.filestore.dao.IFilestoreDao;
import net.jeebiz.cloud.extras.filestore.setup.FilestoreProperties;
import net.jeebiz.cloud.extras.filestore.setup.provider.AliyunOssFilestoreProvider;
import net.jeebiz.cloud.extras.filestore.setup.provider.FastdfsFilestoreProvider;
import net.jeebiz.cloud.extras.filestore.setup.provider.LocalFilestoreProvider;
import net.jeebiz.cloud.extras.filestore.setup.provider.MinioFilestoreProvider;


/**
 * 存储目标（local:服务本地,fdfs:FastDFS存储服务,minio:MinIO对象存储）
 */
@Configuration
public class FilestoreProviderConfiguration {

	@Bean
	@ConditionalOnProperty(prefix = FilestoreProperties.PREFIX, value = "storage", havingValue = "local")
	public LocalFilestoreProvider localFilestoreProvider(IFilestoreDao filestoreDao,
			FilestoreProperties filestoreProperties) {
		return new LocalFilestoreProvider(filestoreProperties.getEndpoint(), filestoreDao, filestoreProperties);
	}
	
	
	@Bean
	@ConditionalOnProperty(prefix = FilestoreProperties.PREFIX, value = "storage", havingValue = "oss")
	public AliyunOssFilestoreProvider aliyunOssFilestoreProvider(IFilestoreDao filestoreDao,
			FilestoreProperties filestoreProperties) {
		return new AliyunOssFilestoreProvider(filestoreProperties.getEndpoint(), filestoreDao);
	}
	
	@Bean
	@ConditionalOnClass(FastFileStorageClient.class)
	@ConditionalOnProperty(prefix = FilestoreProperties.PREFIX, value = "storage", havingValue = "fdfs")
	public FastdfsFilestoreProvider fastdfsFilestoreProvider(IFilestoreDao filestoreDao, FastFileStorageClient fdfsStorageClient,
			FastdfsTemplate fdfsTemplate) {
		return new FastdfsFilestoreProvider(filestoreDao, fdfsStorageClient, fdfsTemplate);
	}
	
	@Bean
	@ConditionalOnClass(MinioClient.class)
	@ConditionalOnProperty(prefix = FilestoreProperties.PREFIX, value = "storage", havingValue = "minio")
	public MinioFilestoreProvider minioFilestoreProvider(IFilestoreDao filestoreDao, MinioClient minioClient,
			FilestoreProperties filestoreProperties) {
		return new MinioFilestoreProvider(filestoreProperties.getEndpoint(), filestoreDao, minioClient);
	}
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.filestore.setup.provider;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

import net.jeebiz.cloud.extras.core.web.vo.FilestoreVo;
import net.jeebiz.cloud.extras.filestore.dao.IFilestoreDao;
import net.jeebiz.cloud.extras.filestore.web.vo.FilestoreDownloadVo;

public class AliyunOssFilestoreProvider implements FilestoreProvider {

	public static final String TO = "oss";
	
	private String endpoint;
	private IFilestoreDao filestoreDao;
	
	public AliyunOssFilestoreProvider(String endpoint, IFilestoreDao filestoreDao) {
		this.endpoint = endpoint;
		this.filestoreDao = filestoreDao;
	}

	@Override
	public String getName() {
		return TO;
	}

	@Override
	public FilestoreVo upload(MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<FilestoreVo> upload(MultipartFile[] files) throws IOException {
		// TODO Auto-generated method stub
		return Lists.newArrayList();
	}

	@Override
	public boolean deleteByPath(List<String> paths) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean deleteByUuid(List<String> uuids) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public FilestoreVo reupload(String uid, MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FilestoreVo> listByPath(List<String> paths) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FilestoreVo> listByUuid(List<String> uuids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FilestoreDownloadVo downloadByPath(String path) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FilestoreDownloadVo downloadByUuid(String uuid) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public IFilestoreDao getFilestoreDao() {
		return filestoreDao;
	}



}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.filestore.web.mvc;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaTypeFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.jeebiz.cloud.api.ApiCode;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.utils.CollectionUtils;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.extras.core.web.vo.FilestoreVo;
import net.jeebiz.cloud.extras.filestore.service.IFilestoreService;
import net.jeebiz.cloud.extras.filestore.web.vo.FilestoreDownloadVo;

@RestController
@RequestMapping("/file/store/")
public class FilestoreController extends BaseApiController {
	
	@Autowired
	private IFilestoreService filestoreService;
	
	@ApiOperation(value = "文件服务：单上传文件", notes = "将单个文件上传到指定的存储对象")
    @PostMapping(value = "single", headers = "content-type=multipart/form-data")
	@PreAuthorize("authenticated")
    public ApiRestResponse<FilestoreVo> upload(@ApiParam(value = "附件文件", required = true) @RequestParam(value = "file")
                                                     @NotNull(message = "文件不能为空") MultipartFile file) throws Exception {
		if (null == file){
			return ApiRestResponse.of(ApiCode.SC_UNSATISFIED_PARAM);
		}
		FilestoreVo result = getFilestoreService().upload(file);
		if (result == null) {
			return fail("file.upload.fail");
		}
		return ApiRestResponse.of(ApiCode.SC_SUCCESS.getCode(), getMessage("file.upload.success"), result);
	}
	
	@ApiOperation(value = "文件服务：多上传文件", notes = "将多个文件上传到指定的存储对象")
    @PostMapping(value = "uploads", headers = "content-type=multipart/form-data")
	@PreAuthorize("authenticated")
    public ApiRestResponse<List<FilestoreVo>> upload(@ApiParam(value = "附件文件", required = true) @RequestParam(value = "files")
                                                     @NotNull(message = "文件不能为空") MultipartFile[] files) throws Exception {
		if (null == files || files.length == 0){
			return ApiRestResponse.of(ApiCode.SC_UNSATISFIED_PARAM);
		}
		List<FilestoreVo> result = getFilestoreService().upload(files);
		if (CollectionUtils.isEmpty(result)) {
			return fail("file.upload.fail");
		}
		return ApiRestResponse.of(ApiCode.SC_SUCCESS.getCode(), getMessage("file.upload.success"), result);
	}
	
	@ApiOperation(value = "文件服务：删除文件", notes = "删除指定存储对象下的指定文件")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "uuid", value = "文件UID", required = true, dataType = "String"),
	})
	@PostMapping("delete")
	@PreAuthorize("authenticated")
	public ApiRestResponse<String> delete(@RequestParam("uuid") @NotEmpty(message = "文件UID不能为空") String uuid) throws Exception {
		try {
			getFilestoreService().deleteByUuid(Arrays.asList(uuid));
			return success("file.delete.success");
		} catch (Exception e) {
			return ApiRestResponse.error(e.getMessage());
		}
	}
	
	@ApiOperation(value = "文件服务：批量删除文件", notes = "批量删除指定存储对象下的指定文件")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "paths", value = "要删除的文件path集合", required = true, dataType = "java.util.List<String>")
	})
	@PostMapping("deleteByPath")
	@PreAuthorize("authenticated")
	public ApiRestResponse<String> deleteByPath( @NotNull(message = "文件路径不能为空") @RequestParam("paths") List<String> paths) throws Exception {
		try {
			getFilestoreService().deleteByPath(paths);
			return success("file.delete.success");
		} catch (Exception e) {
			logException(this, e);
			return ApiRestResponse.error(e.getMessage());
		}
	}

	@ApiOperation(value = "文件服务：批量删除文件", notes = "批量删除指定存储对象下的指定文件")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "uuids", value = "要删除的文件Uuid集合", required = true, dataType = "java.util.List<String>")
	})
	@PostMapping("deleteByUuid")
	@PreAuthorize("authenticated")
	public ApiRestResponse<String> deleteByUuid(@NotNull(message = "文件UUID不能为空") @RequestParam("uuids") List<String> uuids) throws Exception {
		try {
			getFilestoreService().deleteByUuid(uuids);
			return success("file.delete.success");
		} catch (Exception e) {
			return ApiRestResponse.error(e.getMessage());
		}
	}
	
	@ApiOperation(value = "文件服务：重新上传文件", notes = "重新上传指定的文件")
	@ApiImplicitParams({
            @ApiImplicitParam(name = "uuid", value = "原文件UID", required = true, dataType = "String")
    })
    @PostMapping(value = "reupload", headers = "content-type=multipart/form-data")
	@PreAuthorize("authenticated")
    public ApiRestResponse<FilestoreVo> reupload(@RequestParam("uuid")
			@NotEmpty(message = "原文件UUID不能为空") String uuid,
                                                 @ApiParam(value = "文件", required = true)
                                                 @RequestParam("file") @NotNull(message = "文件不能为空") MultipartFile file) throws Exception {
		if (null == file){
			return ApiRestResponse.of(ApiCode.SC_UNSATISFIED_PARAM);
		}
		FilestoreVo result = getFilestoreService().reupload(uuid,file);
		if (result == null) {
			return fail("file.reupload.fail");
		}
		return success("file.reupload.success");
	}
	
	@ApiOperation(value = "文件服务：文件信息", notes = "根据给出的文件相对路径获取文件信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "paths", value = "文件Path集合", required = true, dataType = "java.util.List<String>")
	})
	@PostMapping("listByPath")
	@PreAuthorize("authenticated")
    public ApiRestResponse<List<FilestoreVo>> listByPath(@NotNull(message = "文件路径不能为空") @RequestParam("paths") List<String> paths) throws Exception {
		try {
			return ApiRestResponse.success(getFilestoreService().listByPath(paths));
		} catch (Exception e) {
			return ApiRestResponse.error(e.getMessage());
		}
	}
	
	@ApiOperation(value = "文件服务：文件信息", notes = "根据给出的文件Uuid获取文件信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "uuids", value = "文件Uuid集合", required = true, dataType = "java.util.List<String>")
	})
	@PostMapping("listByUuid")
	@PreAuthorize("authenticated")
    public ApiRestResponse<List<FilestoreVo>> listByUuid(@NotNull(message = "文件UUID不能为空") @RequestParam("uuids") List<String> uuids) throws Exception {
		try {
			return ApiRestResponse.success(getFilestoreService().listByUuid(uuids));
		} catch (Exception e) {
			return ApiRestResponse.error(e.getMessage());
		}
	}

	@ApiOperation(value = "文件服务：文件下载", notes = "根据给出的文件相对路径下载文件")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "path", value = "要下载的文件path", required = true, dataType = "String")
	})
	@PostMapping("downloadByPath")
	@PreAuthorize("authenticated")
	public ResponseEntity<byte[]> downloadByPath(@NotNull(message = "文件路径不能为空") @RequestParam("path") String path) throws Exception {

		ResponseEntity<byte[]> entity = null;
		try {

			FilestoreDownloadVo downloadVo = getFilestoreService().downloadByPath(path);
			if (downloadVo != null) {

				// 定义http头 ，状态
				HttpHeaders header = new HttpHeaders();
				header.add("Content-Disposition", "attchement;filename=" + downloadVo.getName());
				header.setContentType(MediaTypeFactory.getMediaType(downloadVo.getName()).get());

				// 定义ResponseEntity封装返回信息
				return new ResponseEntity<byte[]>(downloadVo.getBytes(), header, HttpStatus.OK);

			}

		} catch (Exception e) {

		}

		return entity;
	}

	@ApiOperation(value = "文件服务：文件下载", notes = "根据给出的文件Uuid下载文件")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "uuid", value = "要下载的文件Uuid", required = true, dataType = "String")
	})
	@PostMapping("downloadByUuid")
	@PreAuthorize("authenticated")
	public ResponseEntity<byte[]> downloadByUuid( @NotNull(message = "文件路径不能为空") @RequestParam("uuid") String uuid) throws Exception {
		ResponseEntity<byte[]> entity = null;
		try {

			FilestoreDownloadVo downloadVo = getFilestoreService().downloadByUuid(uuid);
			if (downloadVo != null) {

				// 定义http头 ，状态
				HttpHeaders header = new HttpHeaders();
				header.add("Content-Disposition", "attchement;filename=" + downloadVo.getName());
				header.setContentType(MediaTypeFactory.getMediaType(downloadVo.getName()).get());

				// 定义ResponseEntity封装返回信息
				return new ResponseEntity<byte[]>(downloadVo.getBytes(), header, HttpStatus.OK);

			}

		} catch (Exception e) {

		}

		return entity;
	}
	
	public IFilestoreService getFilestoreService() {
		return filestoreService;
	}
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.filestore.setup;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(FilestoreProperties.PREFIX)
public class FilestoreProperties {

	public static final String PREFIX = "file";

	/**
	 * 存储服务对外服务的主机地址或域名
	 */
	private String endpoint;
	
	/**
	 * 存储目标（local:服务本地,fdfs:FastDFS存储服务,minio:MinIO对象存储）
	 */
	private String storage = "local";
	
	/**
	 * 本地存储目录
	 */
	private String local = SystemUtils.getUserDir().getAbsolutePath();

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getStorage() {
		return storage;
	}
	
	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

}

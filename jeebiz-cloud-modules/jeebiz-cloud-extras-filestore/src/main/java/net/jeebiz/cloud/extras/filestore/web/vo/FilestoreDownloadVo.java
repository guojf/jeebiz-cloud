package net.jeebiz.cloud.extras.filestore.web.vo;

import java.io.InputStream;
import java.util.Set;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.extras.core.web.vo.FileMetaDataVo;

@ApiModel(value = "FilestoreDownloadVo", description = "文件存储信息Vo")
@Getter
@Setter
@ToString
public class FilestoreDownloadVo {

	/**
	 * 文件UUID
	 */
	private String uuid;
	/**
	 * 文件名称
	 */
	private String name;
	/**
	 * 文件存储路径
	 */
	private String path;
	/**
	 * 文件访问地址
	 */
	private String url;
	/**
	 * 文件类型
	 */
	private String ext;
	/**
	 * 文件元信息
	 */
	private Set<FileMetaDataVo> metadata;
	/*
	 * 文件字节码
	 */
	private byte[] bytes;
	/**
	 * 文件流对象
	 */
	private InputStream stream;

}

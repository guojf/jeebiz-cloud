/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.rbac1.dao.entities;

import org.apache.ibatis.type.Alias;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.dao.entities.PaginationModel;

@Alias(value = "AuthzUserDetailModel")
@SuppressWarnings("serial")
@Getter
@Setter
@ToString
public class AuthzUserDetailModel extends PaginationModel<AuthzUserDetailModel> {

	/**
	 * 用户Id
	 */
	private String id;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 用户别名（昵称）
	 */
	private String alias;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 用户密码盐：用于密码加解密
	 */
	private String salt;
	/**
	 * 用户秘钥：用于用户JWT加解密
	 */
	private String secret;
	/**
	 * 用户头像：图片路径或图标样式
	 */
	private String avatar;
	/**
	 * 手机号码
	 */
	private String phone;
	/**
	 * 电子邮箱
	 */
	private String email;
	/**
	 * 用户备注
	 */
	private String remark;
	/**
	 * 用户状态（0:禁用|1:可用|2:锁定）
	 */
	private String status;
	/**
	 * 初始化时间
	 */
	private String time24;
	 
	/**
	 * 用户详情ID
	 */
	private String dId;
	/**
	 * 性别：（male：男，female：女）
	 */
	private String gender;
	/**
	 * 出生日期
	 */
	private String birthday;
	/**
	 * 身份证号码
	 */
	private String idcard;
	/**
	 * 角色ID（可能多个组合，如：1,2）
	 */
	private String roleId;
	/**
	 * 角色名称（可能多个组合，如：角色1,角色2）
	 */
	private String roleName;
	
}

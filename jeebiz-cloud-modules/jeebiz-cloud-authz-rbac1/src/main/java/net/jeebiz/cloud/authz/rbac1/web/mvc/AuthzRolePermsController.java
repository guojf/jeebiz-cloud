/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.rbac1.web.mvc;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.annotation.BusinessLog;
import net.jeebiz.cloud.api.annotation.BusinessType;
import net.jeebiz.cloud.api.dao.entities.PairModel;
import net.jeebiz.cloud.api.utils.Constants;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.authz.rbac1.dao.entities.AuthzRolePermsModel;
import net.jeebiz.cloud.authz.rbac1.service.IAuthzRolePermsService;
import net.jeebiz.cloud.authz.rbac1.web.vo.AuthzRoleAllotPermsVo;

/**
 * 权限管理：角色功能权限
 */
@Api(tags = "权限管理：角色功能权限（Ok）")
@RestController
@RequestMapping(value = "/authz/role/perms/")
public class AuthzRolePermsController extends BaseApiController {

	@Autowired
	private IAuthzRolePermsService authzRolePermsService;//角色权限管理SERVICE
	
	@ApiOperation(value = "指定角色已授权功能列表（键值对）", notes = "查询指定角色已授权功能列表")
	@ApiImplicitParams({
		@ApiImplicitParam( name = "roleId", required = true, value = "角色ID", dataType = "String")
	})
	@GetMapping("pairs")
	@PreAuthorize("authenticated and (hasAuthority('role:perms') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<List<PairModel>> list(@RequestParam String roleId) throws Exception {
		List<PairModel> roleList = getAuthzRolePermsService().getPairValues(roleId);
		if(roleList == null) {
			roleList = Lists.newArrayList();
		}
		return ApiRestResponse.success(roleList);
	}
	
	@ApiOperation(value = "给指定角色分配功能权限", notes = "给指定角色分配功能权限")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "permsVo", value = "角色分配的功能权限信息", dataType = "AuthzRoleAllotPermsVo")
	})
	@BusinessLog(module = Constants.AUTHZ_ROLE_PERMS, business = "给指定角色分配权限，角色Id：${roleid}", opt = BusinessType.DELETE)
	@PostMapping("perms")
	@PreAuthorize("authenticated and (hasAuthority('role:perms') or hasAuthority('*')) ")
	@ResponseBody
	public Object perms(@Valid @RequestBody AuthzRoleAllotPermsVo permsVo) throws Exception { 
		
		AuthzRolePermsModel permsModel = getBeanMapper().map(permsVo, AuthzRolePermsModel.class);
		int total = getAuthzRolePermsService().doPerms(permsModel);
		if(total > 0) {
			return success("role.perms.success", total); 
		}
		return fail("role.perms.fail", total); 
	}
	
	@ApiOperation(value = "取消已分配给指定角色的权限", notes = "取消已分配给指定角色的权限")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "permsVo", value = "角色取消分配的权限信息", dataType = "AuthzRoleAllotPermsVo")
	})
	@BusinessLog(module = Constants.AUTHZ_ROLE_PERMS, business = "取消已分配给指定角色的权限", opt = BusinessType.DELETE)
	@PostMapping("unperms")
	@PreAuthorize("authenticated and (hasAuthority('role:unperms') or hasAuthority('*')) ")
	@ResponseBody
	public Object unperms(@Valid @RequestBody AuthzRoleAllotPermsVo permsVo) throws Exception { 
		AuthzRolePermsModel permsModel = getBeanMapper().map(permsVo, AuthzRolePermsModel.class);
		int total = getAuthzRolePermsService().unPerms(permsModel);
		if(total > 0) {
			return success("role.unperms.success", total); 
		}
		return fail("role.unperms.fail", total);
	}
	
	public IAuthzRolePermsService getAuthzRolePermsService() {
		return authzRolePermsService;
	}

	public void setAuthzRolePermsService(IAuthzRolePermsService authzRolePermsService) {
		this.authzRolePermsService = authzRolePermsService;
	}

}

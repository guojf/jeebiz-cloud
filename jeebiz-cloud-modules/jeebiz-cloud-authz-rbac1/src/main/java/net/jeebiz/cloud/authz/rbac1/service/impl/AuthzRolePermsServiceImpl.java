/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.rbac1.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.rbac1.dao.IAuthzRolePermsDao;
import net.jeebiz.cloud.authz.rbac1.dao.entities.AuthzRolePermsModel;
import net.jeebiz.cloud.authz.rbac1.service.IAuthzRolePermsService;
import net.jeebiz.cloud.authz.rbac1.utils.AuthzPermsUtils;

@Service
public class AuthzRolePermsServiceImpl extends BaseServiceImpl<AuthzRolePermsModel, IAuthzRolePermsDao>
		implements IAuthzRolePermsService {

	@Override
	public List<String> getPerms(String roleId) {
		return getDao().getPerms(roleId);
	}
	
	@Override
	public int doPerms(AuthzRolePermsModel model) {
		// 查询已经授权标记
		List<String> oldperms = getDao().getPerms(model.getRoleId());
		// 此次提交的授权标记
		List<String> perms = AuthzPermsUtils.distinct(model.getPerms());
		// 之前没有权限
		if(CollectionUtils.isEmpty(oldperms)) {
			// 执行授权
			getDao().setPerms(model.getRoleId(), perms);
			return 1;
		}
		// 之前有权限,这里需要筛选出新增的权限和取消的权限
		else {
			// 授权标记增量
			List<String> increments = AuthzPermsUtils.increment(perms, oldperms);
			if(!CollectionUtils.isEmpty(increments)) {
				getDao().setPerms(model.getRoleId(), increments);
			}
			return 1;
		}
	}

	@Override
	public int unPerms(AuthzRolePermsModel model) {
		// 此次要取消授权的授权标记
		List<String> perms = AuthzPermsUtils.distinct(model.getPerms());
		if(!CollectionUtils.isEmpty(perms)) {
			getDao().delPerms(model.getRoleId(), perms);
		}
		return 1;
	}

}

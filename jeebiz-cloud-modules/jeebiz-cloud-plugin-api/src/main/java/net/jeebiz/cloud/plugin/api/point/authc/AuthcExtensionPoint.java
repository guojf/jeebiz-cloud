package net.jeebiz.cloud.plugin.api.point.authc;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.pf4j.ExtensionPoint;
import org.pf4j.PluginException;

public interface AuthcExtensionPoint extends ExtensionPoint {

	String getToken(HttpServletRequest request, Map<String, Object> params) throws PluginException;

	void handleHeader(HttpServletRequest request, Map<String, Object> params) throws PluginException;

	void handleRequest(HttpServletRequest request, Map<String, Object> params) throws PluginException;

	Object handleResult(Object res) throws PluginException;

}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.feature.setup.handler;

import java.util.List;

import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureModel;
import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureOptModel;

public interface FeatureDataHandler<T> {

	T handle(List<AuthzFeatureModel> featureList, List<AuthzFeatureOptModel> featureOptList);
	
}

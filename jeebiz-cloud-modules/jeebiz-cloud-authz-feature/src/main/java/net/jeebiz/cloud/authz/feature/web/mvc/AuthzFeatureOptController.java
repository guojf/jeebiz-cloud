/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.feature.web.mvc;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.annotation.BusinessLog;
import net.jeebiz.cloud.api.annotation.BusinessType;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureOptModel;
import net.jeebiz.cloud.authz.feature.service.IAuthzFeatureOptService;
import net.jeebiz.cloud.authz.feature.setup.Constants;
import net.jeebiz.cloud.authz.feature.web.vo.AuthzFeatureOptNewVo;
import net.jeebiz.cloud.authz.feature.web.vo.AuthzFeatureOptRenewVo;
import net.jeebiz.cloud.authz.feature.web.vo.AuthzFeatureOptVo;

@Api(tags = "功能操作：数据维护（Ok）")
@RestController
@RequestMapping(value = "/authz/feature/opt/")
public class AuthzFeatureOptController extends BaseApiController {

	@Autowired
	protected IAuthzFeatureOptService authzFeatureOptService;
	
	@ApiOperation(value = "增加功能操作代码信息", notes = "增加功能操作代码信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "optVo", value = "功能操作代码信息", required = true, dataType = "AuthzFeatureOptNewVo")
	})
	@BusinessLog(module = Constants.AUTHZ_FEATURE_OPT, business = Constants.Biz.AUTHZ_FEATURE_OPT_NEW, opt = BusinessType.INSERT)
	@PostMapping("new")
	@PreAuthorize("authenticated and (hasAuthority('opt:new') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<String> newOpt(@Valid @RequestBody AuthzFeatureOptNewVo optVo) throws Exception {
		int count = getAuthzFeatureOptService().getOptCountByName(optVo.getName(), optVo.getFeatureId(), null);
		if(count > 0) {
			return fail("opt.new.name-exists");
		}
		AuthzFeatureOptModel model = getBeanMapper().map(optVo, AuthzFeatureOptModel.class);
		int total = getAuthzFeatureOptService().insert(model);
		if(total > 0) {
			return success("opt.new.success", total);
		}
		return fail("opt.new.fail", total);
	}
	
	@ApiOperation(value = "修改功能操作代码", notes = "修改功能操作代码信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "optVo", value = "功能操作代码信息", required = true, dataType = "AuthzFeatureOptRenewVo")
	})
	@BusinessLog(module = Constants.AUTHZ_FEATURE_OPT, business = Constants.Biz.AUTHZ_FEATURE_OPT_RENEW, opt = BusinessType.UPDATE)
	@PostMapping("renew")
	@PreAuthorize("authenticated and (hasAuthority('opt:renew') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<String> renewOpt(@Valid @RequestBody AuthzFeatureOptRenewVo optVo) throws Exception {
		int count = getAuthzFeatureOptService().getOptCountByName(optVo.getName(), optVo.getFeatureId(), optVo.getId());
		if(count > 0) {
			return fail("opt.new.name-exists");
		}
		AuthzFeatureOptModel model = getBeanMapper().map(optVo, AuthzFeatureOptModel.class);
		int total = getAuthzFeatureOptService().update(model);
		if(total > 0) {
			return success("opt.renew.success", total);
		}
		return fail("opt.renew.fail", total);
	}
	
	@ApiOperation(value = "查询功能操作代码信息", notes = "根据功能操作代码ID查询功能操作代码信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam( paramType = "path", name = "id", required = true, value = "功能操作代码ID", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_FEATURE_OPT, business = Constants.Biz.AUTHZ_FEATURE_OPT_DETAIL, opt = BusinessType.SELECT)
	@GetMapping("detail/{id}")
	@PreAuthorize("authenticated and (hasAuthority('opt:detail') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<AuthzFeatureOptVo> detail(@PathVariable("id") String id) throws Exception {
		AuthzFeatureOptModel model = getAuthzFeatureOptService().getModel(id);
		if( model == null) {
			return ApiRestResponse.empty(getMessage("opt.get.empty"));
		}
		return ApiRestResponse.success(getBeanMapper().map(model, AuthzFeatureOptVo.class));
	}
	
	@ApiOperation(value = "删除功能操作代码信息", notes = "删除功能操作代码信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam( paramType = "path", name = "id", required = true, value = "功能操作代码ID", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_FEATURE_OPT, business = Constants.Biz.AUTHZ_FEATURE_OPT_DETAIL, opt = BusinessType.DELETE)
	@GetMapping("delete/{id}")
	@PreAuthorize("authenticated and (hasAuthority('opt:delete') or hasAuthority('*')) ")
	@ResponseBody
	public ApiRestResponse<String> delOpt(@PathVariable String id) throws Exception { 
		int total = getAuthzFeatureOptService().delete(id);
		if(total > 0) {
			return success("opt.delete.success", total);
		}
		return success("opt.delete.fail", total);
	}

	public IAuthzFeatureOptService getAuthzFeatureOptService() {
		return authzFeatureOptService;
	}

	public void setAuthzFeatureOptService(IAuthzFeatureOptService authzFeatureOptService) {
		this.authzFeatureOptService = authzFeatureOptService;
	}
	
}

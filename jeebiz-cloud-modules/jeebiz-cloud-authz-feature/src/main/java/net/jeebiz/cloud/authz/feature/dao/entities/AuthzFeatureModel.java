/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.feature.dao.entities;

import org.apache.ibatis.type.Alias;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.dao.entities.BaseModel;

/**
 * 功能菜单信息表
 */
@SuppressWarnings("serial")
@Alias("AuthzFeatureModel")
@Getter
@Setter
@ToString
public class AuthzFeatureModel extends BaseModel<AuthzFeatureModel> {

	/**
	 * 功能菜单Id
	 */
	private String id;
	/**
	 * 功能菜单名称
	 */
	private String name;
	/**
	 * 功能菜单简称
	 */
	private String abb;
	/**
	 * 功能菜单编码：用于与功能操作代码组合出权限标记以及作为前段判断的依据
	 */
	private String code;
	/**
	 * 功能菜单URL
	 */
	private String url;
	/**
	 * 菜单类型(1:原生|2:自定义)
	 */
	private String type;
	/**
	 * 菜单样式或菜单图标路径
	 */
	private String icon;
	/**
	 * 菜单显示顺序
	 */
	private String order;
	/**
	 * 父级功能菜单ID
	 */
	private String parent;
	/**
	 * 菜单是否可见(1:可见|0:不可见)
	 */
	private String visible;
	/**
	 * 菜单所拥有的权限标记
	 */
	private String perms;

}

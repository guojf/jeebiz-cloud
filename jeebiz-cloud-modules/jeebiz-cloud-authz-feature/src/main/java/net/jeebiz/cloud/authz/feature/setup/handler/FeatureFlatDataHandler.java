/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.feature.setup.handler;

import java.util.List;

import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureModel;
import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureOptModel;
import net.jeebiz.cloud.authz.feature.utils.FeatureNavUtils;
import net.jeebiz.cloud.authz.feature.web.vo.AuthzFeatureVo;

public class FeatureFlatDataHandler implements FeatureDataHandler<List<AuthzFeatureVo>> {

	@Override
	public List<AuthzFeatureVo> handle(List<AuthzFeatureModel> featureList, List<AuthzFeatureOptModel> featureOptList) {
		return FeatureNavUtils.getFeatureFlatList(featureList, featureOptList);
	}

}

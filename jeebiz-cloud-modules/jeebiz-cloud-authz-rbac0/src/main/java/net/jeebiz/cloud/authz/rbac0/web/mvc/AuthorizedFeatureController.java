/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.rbac0.web.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.boot.biz.userdetails.SecurityPrincipal;
import org.springframework.security.boot.utils.SubjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureModel;
import net.jeebiz.cloud.authz.feature.dao.entities.AuthzFeatureOptModel;
import net.jeebiz.cloud.authz.feature.service.IAuthzFeatureOptService;
import net.jeebiz.cloud.authz.feature.service.IAuthzFeatureService;
import net.jeebiz.cloud.authz.feature.setup.handler.FeatureDataHandlerFactory;
import net.jeebiz.cloud.authz.feature.web.vo.AuthzFeatureVo;
import net.jeebiz.cloud.authz.rbac0.service.IAuthorizedFeatureService;

@Api(tags = "功能菜单：数据维护（Ok）")
@RestController
@RequestMapping(value = "/authorized/feature/")
public class AuthorizedFeatureController extends BaseApiController {

	@Autowired
	private IAuthzFeatureService authzFeatureService;
	@Autowired
	private IAuthzFeatureOptService authzFeatureOptService;
	@Autowired
	private IAuthorizedFeatureService authorizedFeatureService;
	 
	@ApiOperation(value = "服务功能菜单-树形结构数据", notes = "根据服务ID及等登录人信息查询该服务的功能菜单-树形结构数据")
	@GetMapping("tree")
	@PreAuthorize("authenticated")
	public ApiRestResponse<List<AuthzFeatureVo>> tree(){
		// 登录账号信息
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		// 所有的功能菜单
		List<AuthzFeatureModel> featureList = getAuthorizedFeatureService().getFeatures(principal.getRoleid());
		// 所有的功能菜单
		List<AuthzFeatureOptModel> featureOptList = getAuthorizedFeatureService().getFeatureOpts(principal.getRoleid());
		// 返回各级菜单 + 对应的功能权限数据
		return ApiRestResponse.success(FeatureDataHandlerFactory.getTreeHandler().handle(featureList, featureOptList));
	}

	@ApiOperation(value = "服务功能菜单-树形结构数据", notes = "根据服务ID及等登录人信息查询该服务的功能菜单-树形结构数据")
	@ApiImplicitParams({
		@ApiImplicitParam( name = "servId", required = false, value = "服务ID", dataType = "String")
	})
	@GetMapping("children")
	@PreAuthorize("authenticated")
	public ApiRestResponse<AuthzFeatureVo> children(@RequestParam String servId){
		// 登录账号信息
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		// 所有的功能菜单
		AuthzFeatureVo featureTree = getAuthorizedFeatureService().getChildFeatures(principal.getRoleid(), servId);
		// 返回各级菜单 + 对应的功能权限数据
		return ApiRestResponse.success(featureTree);
	}
	
	public IAuthzFeatureService getAuthzFeatureService() {
		return authzFeatureService;
	}

	public IAuthzFeatureOptService getAuthzFeatureOptService() {
		return authzFeatureOptService;
	}

	public IAuthorizedFeatureService getAuthorizedFeatureService() {
		return authorizedFeatureService;
	}
	
}

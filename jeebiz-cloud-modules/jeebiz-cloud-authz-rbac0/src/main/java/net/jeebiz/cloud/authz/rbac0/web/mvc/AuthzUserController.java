/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.rbac0.web.mvc;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.boot.biz.userdetails.SecurityPrincipal;
import org.springframework.security.boot.utils.SubjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.annotation.BusinessLog;
import net.jeebiz.cloud.api.annotation.BusinessType;
import net.jeebiz.cloud.api.utils.Constants;
import net.jeebiz.cloud.api.utils.StringUtils;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.api.webmvc.Result;
import net.jeebiz.cloud.authz.rbac0.dao.entities.AuthzRoleModel;
import net.jeebiz.cloud.authz.rbac0.dao.entities.AuthzUserAllotRoleModel;
import net.jeebiz.cloud.authz.rbac0.dao.entities.AuthzUserDetailModel;
import net.jeebiz.cloud.authz.rbac0.dao.entities.AuthzUserModel;
import net.jeebiz.cloud.authz.rbac0.service.IAuthzUserService;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzRoleVo;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzUserAllotRoleVo;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzUserDetailNewVo;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzUserDetailRenewVo;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzUserDetailVo;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzUserPaginationVo;
import net.jeebiz.cloud.authz.rbac0.web.vo.AuthzUserResetVo;

/**
 * 权限管理：用户管理
 */
@Api(tags = "权限管理：用户管理（Ok）")
@RestController
@RequestMapping(value = "/authz/user")
public class AuthzUserController extends BaseApiController {

	@Autowired
	private IAuthzUserService authzUserService;//用户管理SERVICE
	
	@ApiOperation(value = "分页查询用户信息", notes = "分页查询用户信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "paginationVo", value = "用户信息筛选条件", dataType = "AuthzUserPaginationVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "分页查询用户信息", opt = BusinessType.SELECT)
	@PostMapping("list")
	@PreAuthorize("authenticated and (hasAuthority('user:list') or hasAuthority('*')) ")
	@ResponseBody
	public Object list(@Valid @RequestBody AuthzUserPaginationVo paginationVo){
		
		AuthzUserDetailModel model = getBeanMapper().map(paginationVo, AuthzUserDetailModel.class);
		Page<AuthzUserDetailModel> pageResult = getAuthzUserService().getPagedList(model);
		List<AuthzUserDetailVo> retList = new ArrayList<AuthzUserDetailVo>();
		for (AuthzUserDetailModel detailModel : pageResult.getRecords()) {
			retList.add(getBeanMapper().map(detailModel, AuthzUserDetailVo.class));
		}
		
		return new Result<AuthzUserDetailVo>(pageResult, retList);
	}
	
	@ApiOperation(value = "指定用户详情", notes = "根据用户ID查询用户信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam( paramType = "path", name = "id", required = true, value = "用户ID", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "查询用户-ID：${userid}", opt = BusinessType.SELECT)
	@GetMapping("detail/{id}")
	@PreAuthorize("authenticated and (hasAuthority('user:detail') or hasAuthority('*')) ")
	@ResponseBody
	public Object detail(@PathVariable String id) throws Exception {
		AuthzUserDetailModel model = getAuthzUserService().getModel(id);
		if(model == null) {
			return ApiRestResponse.empty(getMessage("user.get.empty"));
		}
		return getBeanMapper().map(model, AuthzUserDetailVo.class);
	}
	
	@ApiOperation(value = "根据认证信息中的用户ID查询用户详情", notes = "根据认证信息中的用户ID查询用户详情")
	@BusinessLog(module = Constants.AUTHZ_USER, business = "根据认证信息中的用户ID查询用户详情", opt = BusinessType.SELECT)
	@GetMapping("detail")
	@PreAuthorize("authenticated")
	@ResponseBody
	public Object detail() throws Exception { 
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		AuthzUserDetailModel model = getAuthzUserService().getModel(principal.getUserid());
		if(model == null) {
			return ApiRestResponse.empty(getMessage("user.get.empty"));
		}
		return getBeanMapper().map(model, AuthzUserDetailVo.class);
	}
	
	@ApiOperation(value = "增加用户信息", notes = "增加用户信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "userVo", value = "用户信息", required = true, dataType = "AuthzUserDetailNewVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "新增用户-名称：${name}", opt = BusinessType.INSERT)
	@PostMapping("new")
	@PreAuthorize("authenticated and (hasAuthority('user:new') or hasAuthority('*')) ")
	@ResponseBody
	public Object newUser(@Valid @RequestBody AuthzUserDetailNewVo userVo) throws Exception { 
		int total = getAuthzUserService().getCountByName(userVo.getUsername());
		if(total > 0) {
			return fail("user.new.exists");
		}
		AuthzUserDetailModel model = getBeanMapper().map(userVo, AuthzUserDetailModel.class);
		int result = getAuthzUserService().insert(model);
		if(result == 1) {
			return success("user.new.success", result);
		}
		return fail("user.new.fail", result);
	}
	
	@ApiOperation(value = "修改用户信息", notes = "修改用户信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "userVo", value = "用户信息", required = true, dataType = "AuthzUserDetailRenewVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "修改用户-名称：${name}", opt = BusinessType.UPDATE)
	@PostMapping("renew")
	@PreAuthorize("authenticated and (hasAuthority('user:renew') or hasAuthority('*')) ")
	@ResponseBody
	public Object renew(@Valid @RequestBody AuthzUserDetailRenewVo userVo) throws Exception { 
		AuthzUserDetailModel model = getBeanMapper().map(userVo, AuthzUserDetailModel.class);
		int result = getAuthzUserService().update(model);
		if(result == 1) {
			return success("user.renew.success", result);
		}
		return fail("user.renew.fail", result);
	}
	
	@ApiOperation(value = "更新用户状态", notes = "更新用户状态")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "form", name = "id", required = true, value = "用户ID", dataType = "String"),
		@ApiImplicitParam(paramType = "form", name = "status", required = true, value = "用户状态", dataType = "String", allowableValues = "1,0")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "更新用户状态", opt = BusinessType.UPDATE)
	@PostMapping("status")
	@PreAuthorize("authenticated and (hasAuthority('user:status') or hasAuthority('*')) ")
	@ResponseBody
	public Object status(@RequestParam String id, @RequestParam String status) throws Exception {
		int result = getAuthzUserService().setStatus(id, status);
		if(result == 1) {
			return success("user.status.success", result);
		}
		return fail("user.status.fail", result);
	}
	
	@ApiOperation(value = "删除用户信息", notes = "删除用户信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "path", name = "id", value = "基础数据ID", required = true, dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "删除用户-名称：${userid}", opt = BusinessType.DELETE)
	@GetMapping("delete/{id}")
	@PreAuthorize("authenticated and (hasAuthority('user:delete') or hasAuthority('*')) ")
	@ResponseBody
	public Object delUser(@PathVariable String id) throws Exception {
		int total = getAuthzUserService().delete(id);
		if(total > 0) {
			return success("user.delete.success", total); 
		}
		return fail("user.delete.fail", total);
	}
	
	@ApiOperation(value = "删除用户信息（支持批量）", notes = "删除用户信息（支持批量）")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "form", name = "ids", value = "基础数据ID,多个用,拼接", required = true, dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "删除用户-名称：${userid}", opt = BusinessType.DELETE)
	@PostMapping("delete")
	@PreAuthorize("authenticated and (hasAuthority('user:delete') or hasAuthority('*')) ")
	@ResponseBody
	public Object delUsers(@RequestParam String ids) throws Exception {
		// 执行基础数据删除操作
		List<String> idList = Lists.newArrayList(StringUtils.tokenizeToStringArray(ids));
		int total = getAuthzUserService().batchDelete(idList);
		if(total > 0) {
			return success("user.delete.success", total); 
		}
		return fail("user.delete.fail", total);
	}
	
	@ApiOperation(value = "分页查询用户已分配角色信息", notes = "分页查询用户已分配角色信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "paginationVo", value = "已分配角色信息筛选条件", dataType = "AuthzUserPaginationVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "分页查询用户已分配角色信息,用户Id：${userid}", opt = BusinessType.DELETE)
	@PostMapping("allocated")
	@PreAuthorize("authenticated and (hasAuthority('user:allocated') or hasAuthority('*')) ")
	@ResponseBody
	public Object allocated(@Valid @RequestBody AuthzUserPaginationVo paginationVo){
		
		AuthzUserModel model = getBeanMapper().map(paginationVo, AuthzUserModel.class);
		Page<AuthzRoleModel> pageResult = getAuthzUserService().getPagedAllocatedList(model);
		List<AuthzRoleVo> retList = new ArrayList<AuthzRoleVo>();
		for (AuthzRoleModel userModel : pageResult.getRecords()) {
			retList.add(getBeanMapper().map(userModel, AuthzRoleVo.class));
		}
		return new Result<AuthzRoleVo>(pageResult, retList);
	}
	
	@ApiOperation(value = "分页查询用户未分配角色信息", notes = "分页查询用户未分配角色信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "paginationVo", value = "未分配角色信息筛选条件", dataType = "AuthzUserPaginationVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "分页查询用户未分配角色信息,用户Id：${userid}", opt = BusinessType.DELETE)
	@PostMapping("unallocated")
	@PreAuthorize("authenticated and (hasAuthority('user:unallocated') or hasAuthority('*')) ")
	@ResponseBody
	public Object unallocated(@Valid @RequestBody AuthzUserPaginationVo paginationVo){
		
		AuthzUserModel model = getBeanMapper().map(paginationVo, AuthzUserModel.class);
		Page<AuthzRoleModel> pageResult = getAuthzUserService().getPagedUnAllocatedList(model);
		List<AuthzRoleVo> retList = new ArrayList<AuthzRoleVo>();
		for (AuthzRoleModel userModel : pageResult.getRecords()) {
			retList.add(getBeanMapper().map(userModel, AuthzRoleVo.class));
		}
		return new Result<AuthzRoleVo>(pageResult, retList);
	}
	
	@ApiOperation(value = "给指定用户分配角色", notes = "给指定用户分配角色")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "allotVo", value = "用户分配的角色信息", dataType = "AuthzUserAllotRoleVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "给指定用户分配角色，用户Id：${userid}", opt = BusinessType.DELETE)
	@PostMapping("allot")
	@PreAuthorize("authenticated and (hasAuthority('user:allot') or hasAuthority('*')) ")
	@ResponseBody
	public Object allot(@Valid @RequestBody AuthzUserAllotRoleVo allotVo) throws Exception { 
		AuthzUserAllotRoleModel model = getBeanMapper().map(allotVo, AuthzUserAllotRoleModel.class);
		int total = getAuthzUserService().doAllot(model);
		return success("user.allot.success", total); 
	}
	
	@ApiOperation(value = "取消已分配给指定用户的角色", notes = "取消已分配给指定用户的角色")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "allotVo", value = "用户取消分配的角色信息", dataType = "AuthzUserAllotRoleVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "取消已分配给指定用户的角色，用户Id：${userid}", opt = BusinessType.DELETE)
	@PostMapping("unallot")
	@PreAuthorize("authenticated and (hasAuthority('user:unallot') or hasAuthority('*')) ")
	@ResponseBody
	public Object unallot(@Valid @RequestBody AuthzUserAllotRoleVo allotVo) throws Exception { 
		AuthzUserAllotRoleModel model = getBeanMapper().map(allotVo, AuthzUserAllotRoleModel.class);
		int total = getAuthzUserService().doUnAllot(model);
		return success("user.unallot.success", total); 
	}
	
	/*------------个人功能-----------------------*/
	
	@ApiOperation(value = "user:reset-info", notes = "设置个人信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "resetVo", value = "用户信息", dataType = "AuthzUserResetVo")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "设置个人信息-名称：${username}", opt = BusinessType.UPDATE)
	@PostMapping("reset/info")
	@PreAuthorize("authenticated")
	@ResponseBody
	public Object resetInfo(@Valid @RequestBody AuthzUserResetVo resetVo) throws Exception { 
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		AuthzUserDetailModel model = getBeanMapper().map(resetVo, AuthzUserDetailModel.class);
		model.setId(principal.getUserid());
		int total = getAuthzUserService().update(model);
		if(total > 0) {
			return success("user.reset.info.success", total); 
		}
		return fail("user.reset.info.fail", total);
	}
	
	@ApiOperation(value = "重置密码：当前登录用户", notes = "重置当前登录用户密码")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "oldPassword", required = true, value = "当前密码", dataType = "String"),
		@ApiImplicitParam(name = "password", required = true, value = "新密码", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_USER, business = "设置密码", opt = BusinessType.UPDATE)
	@PostMapping("reset/pwd")
	@PreAuthorize("authenticated")
	@ResponseBody
	public Object resetPwd(@RequestParam String oldPassword, @RequestParam String password) throws Exception {
		// 密码加密
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		int total = getAuthzUserService().resetPwd(principal.getUserid(), oldPassword, password);
		if(total > 0) {
			return success("user.reset.pwd.success", total); 
		}
		return fail("user.reset.pwd.fail", total);
	}
	
	@ApiOperation(value = "角色权限标记：当前登录用户", notes = "查询已分配给当前用户所属角色的权限")
	@BusinessLog(module = Constants.AUTHZ_USER, business = "查询已分配给当前用户所属角色的权限", opt = BusinessType.SELECT)
	@GetMapping("perms")
	@PreAuthorize("authenticated")
	@ResponseBody
	public Object perms() throws Exception { 
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		return ApiRestResponse.success(getAuthzUserService().getPermissions(principal.getUserid()));
	}
	
	public IAuthzUserService getAuthzUserService() {
		return authzUserService;
	}

	public void setAuthzUserService(IAuthzUserService authzUserService) {
		this.authzUserService = authzUserService;
	}
	
}

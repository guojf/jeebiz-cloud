package com.jeefw.cloud.httppdf.utils;

import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfHeaderFooter extends PdfPageEventHelper {
	public static final String TAG_PAGE_NO = "#PAGENO";

	protected List<Element> header = new ArrayList<Element>();
	protected List<Element> footer = new ArrayList<Element>();
	private Rectangle headerRect;
	private Rectangle footerRect;

	public PdfHeaderFooter() {
	}

	public PdfHeaderFooter(String header, String footer) {
		this.header.add(new Phrase(header));
		this.footer.add(new Phrase(footer));
	}

	public PdfHeaderFooter(Element header, Element footer) {
		this.header.add(header);
		this.footer.add(footer);
	}

	public void addHeader(Element header) {
		this.header.add(header);
	}

	public void addHeader(String header, Font font) {
		this.header.add(new Chunk(header, font));
	}

	public void addHeader(String header, Font font, int alignment) {
		Paragraph pg = new Paragraph(header, font);
		pg.setAlignment(alignment);
		this.header.add(pg);
	}

	public void addFooter(Element footer) {
		this.footer.add(footer);
	}

	public void addFooter(String footer, Font font) {
		this.footer.add(new Chunk(footer, font));
	}

	public void addFooter(String footer, Font font, int alignment) {
		Paragraph pg = new Paragraph(footer, font);
		pg.setAlignment(alignment);
		this.footer.add(pg);
	}

	public Element formatContent(Element e, String tag, String value) {
		if (e instanceof Paragraph) {
			Paragraph pg = (Paragraph) e;
			Paragraph newPg = new Paragraph(
					pg.getContent().replace(tag, value), pg.getFont());
			newPg.setAlignment(pg.getAlignment());
			return newPg;
		} else if (e instanceof Chunk) {
			Chunk ck = (Chunk) e;
			return new Chunk(ck.getContent().replace(tag, value), ck.getFont());
		} else if (e instanceof Phrase) {
			Phrase ph = (Phrase) e;
			return new Phrase(ph.getContent().replace(tag, value), ph.getFont());
		} else {
			return e;
		}
	}

	public void onEndPage(PdfWriter writer, Document document) {
		try {
			String pageNo = String.valueOf(writer.getPageNumber());
			Rectangle hdRect = headerRect == null ? new Rectangle(30, 832, 559,
					810) : headerRect;
			ColumnText ct = new ColumnText(writer.getDirectContent());
			for (Element e : header) {
				ct.setSimpleColumn(hdRect);
				ct.addElement(formatContent(e, TAG_PAGE_NO, pageNo));
				ct.go();
			}

			Rectangle ftRect = footerRect == null ? new Rectangle(30, 10, 559,
					32) : footerRect;
			for (Element e : footer) {
				ct.setSimpleColumn(ftRect);
				ct.addElement(formatContent(e, TAG_PAGE_NO, pageNo));
				ct.go();
			}
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	public List<Element> getHeader() {
		return header;
	}

	public List<Element> getFooter() {
		return footer;
	}

	public Rectangle getHeaderRect() {
		return headerRect;
	}

	public void setHeaderRect(Rectangle headerRect) {
		this.headerRect = headerRect;
	}

	public Rectangle getFooterRect() {
		return footerRect;
	}

	public void setFooterRect(Rectangle footerRect) {
		this.footerRect = footerRect;
	}
}
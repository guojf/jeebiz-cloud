package com.jeefw.cloud.httppdf.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BadPdfFormatException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PRIndirectReference;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

/**
 * 
 * @类名称:PdfUtils.java
 * @类描述：PDF输出相关通用方法
 * @创建人：WuXinfeng
 * @创建时间：2017年1月22日 下午2:49:30
 * @版本号:v1.0
 */
public final class PdfExportUtils {
	/**
	 * 
	 * @描述：查找字体
	 * @创建人:WuXinfeng
	 * @创建时间:2017年1月22日下午2:51:04
	 * @修改人:
	 * @修改时间:
	 * @修改描述:
	 * @param reader
	 * @param fontname
	 * @return
	 */
	public static BaseFont findFontInForm(PdfReader reader, PdfName fontName) {
		PdfDictionary root = reader.getCatalog();
		PdfDictionary acroform = root.getAsDict(PdfName.ACROFORM);
		if (acroform == null)
			return null;
		PdfDictionary dr = acroform.getAsDict(PdfName.DR);
		if (dr == null)
			return null;
		PdfDictionary font = dr.getAsDict(PdfName.FONT);
		if (font == null)
			return null;
		for (PdfName key : font.getKeys()) {
			if (key.equals(fontName)) {
				return BaseFont.createFont((PRIndirectReference) font
						.getAsIndirectObject(key));
			}
		}
		return null;
	}

	/**
	 * 
	 * @描述：替换PDF中的图片
	 * @创建人:WuXinfeng
	 * @创建时间:2017年1月22日下午3:31:29
	 * @修改人:
	 * @修改时间:
	 * @修改描述:
	 * @param pdfReader
	 * @param imageName
	 * @param image
	 * @throws IOException
	 * @throws BadPdfFormatException
	 */
	public static void replacePdfImage(PdfReader reader, PdfName imageName,
			Image image) throws IOException, BadPdfFormatException {
		PdfImage pdfImage = new PdfImage(Image.getInstance(image),
				(imageName == null) ? null : imageName.toString(), null);
		for (int i = 1, iSize = reader.getNumberOfPages(); i <= iSize; i++) {
			PdfDictionary xobjects = reader.getPageN(i)
					.getAsDict(PdfName.RESOURCES).getAsDict(PdfName.XOBJECT);

			Iterator<PdfName> iter = xobjects.getKeys().iterator();
			while (iter.hasNext()) {
				PdfName imgRef = iter.next();
				if ((imageName == null) || (imageName.equals(imgRef))) {
					// 替换图片
					PRStream prStream = (PRStream) xobjects.getAsStream(imgRef);
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try {
						prStream.clear();
						pdfImage.writeContent(baos);
						prStream.setData(baos.toByteArray(), false);
						for (PdfName name : pdfImage.getKeys()) {
							prStream.put(name, pdfImage.get(name));
						}
					} finally {
						baos.close();
					}
					break;
				}
			}
		}
	}

	/**
	 * 
	 * @描述：给表单中各字段赋值
	 * @创建人:WuXinfeng
	 * @创建时间:2017年1月22日下午3:36:42
	 * @修改人:
	 * @修改时间:
	 * @修改描述:
	 * @param reader
	 * @param model
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static ByteArrayOutputStream fillValueInForm(PdfReader reader,
			Object model, BaseFont bf) throws DocumentException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PdfStamper stamper = new PdfStamper(reader, baos);
		AcroFields acroForm = stamper.getAcroFields();

		BaseFont bfChinese = bf == null ? ChineseFont.SIMSUM.getBaseFont() : bf;
		acroForm.addSubstitutionFont(bfChinese);
		// 填充各字段值
		Iterator<String> it = acroForm.getFields().keySet().iterator();
		while (it.hasNext()) {
			String name = it.next().toString();
			try {
				String value = (String) PropertyUtils.getProperty(model, name);
				if (StringUtils.isNotEmpty(value)) {
					acroForm.setFieldProperty(name, "textfont", bfChinese, null);
					acroForm.setField(name, value);
				}
			} catch (Exception ex) {
			}
		}
		// 如果为false那么生成的PDF文件还能编辑
		stamper.setFormFlattening(true);
		stamper.close();
		reader.close();

		return baos;
	}
	
	public static ByteArrayOutputStream formatXhtmlToPdf(InputStream in, Document doc) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		return baos;
	}
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.api;


import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * model for interacting with client.
 */
@ApiModel(value = "ApiRestResponse", description = "接口响应对象")
public class ApiRestResponse<T> {

	@ApiModelProperty(name = "code", dataType = "String", value = "编码")
    private final String code;
	
	@ApiModelProperty(name = "msg", dataType = "String", value = "消息")
    private final String msg;
    
	@ApiModelProperty(name = "data", dataType = "java.lang.Object", value = "数据")
    private T data;

	public ApiRestResponse() {
		this.code = ApiCode.SC_SUCCESS.getCode();
		this.msg = ApiCode.SC_SUCCESS.getReason();
    }
 
    protected ApiRestResponse(final String code, final String msg) {
        this.code = code;
        this.msg = msg;
    }
    
    protected ApiRestResponse(final String code, final String msg, final T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

	public static <T> ApiRestResponse<T> empty(final String msg) {
		return of(ApiCode.SC_EMPTY.getCode(), msg);
	}
    
    public static <T> ApiRestResponse<T> success(final String msg) {
        return of(ApiCode.SC_SUCCESS.getCode(), msg);
    }
    
    public static <T> ApiRestResponse<T> success(final T data) {
        return of(ApiCode.SC_SUCCESS.getCode(), ApiCode.SC_SUCCESS.getReason(), data);
    }
    
    public static <T> ApiRestResponse<T> fail(final String msg) {
        return of(ApiCode.SC_FAIL.getCode(), msg);
    }
    
    public static <T> ApiRestResponse<T> fail(final T data) {
        return of(ApiCode.SC_FAIL.getCode(), ApiCode.SC_FAIL.getReason(), data);
    }
    
    public static <T> ApiRestResponse<T> error(final String msg) {
        return of(ApiCode.SC_INTERNAL_SERVER_ERROR.getCode(), msg);
    }
    
    public static <T> ApiRestResponse<T> error(final T data) {
        return of(ApiCode.SC_INTERNAL_SERVER_ERROR.getCode(), ApiCode.SC_INTERNAL_SERVER_ERROR.getReason(), data);
    }
    
    public static <T> ApiRestResponse<T> of(final ApiCode code) {
        return of(code.getCode(), code.getReason());
    }
    
    public static <T> ApiRestResponse<T> of(final ApiCode code, final String msg) {
        return of(code.getCode(), msg);
    }
    
    public static <T> ApiRestResponse<T> of(final ApiCode code, final T data) {
        return of(code.getCode(), code.getReason(), data);
    }
    
    public static <T> ApiRestResponse<T> of(final int code, final String msg) {
        return of(String.valueOf(code), msg);
    }
    
    public static <T> ApiRestResponse<T> of(final String code, final String msg) {
        return new ApiRestResponse<T>(code, msg);
    }
    
    public static <T> ApiRestResponse<T> of(final String code, final String msg, final T data) {
        return new ApiRestResponse<T>(code, msg, data);
    }

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public T getData() {
		return data;
	}
    
	public Map<String, Object> toMap(){
		Map<String, Object> rtMap = new HashMap<String, Object>();
		rtMap.put("code", code);
		rtMap.put("msg", msg);
		rtMap.put("data", data);
		return rtMap;
	}
	
}

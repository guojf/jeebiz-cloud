/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.api.dao.entities;

import com.baomidou.mybatisplus.extension.activerecord.Model;

@SuppressWarnings("serial")
public class BaseModel<T extends Model<?>> extends Model<T> implements Cloneable {

	/**
	 * 应用唯一ID
	 */
	private String appid;

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

}

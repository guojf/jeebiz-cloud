/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.setup;

public class Constants {
	
	public static final String EXTRAS_INFORM = "Extras-Inform";
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.web.vo;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.SafeHtml;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.annotation.AllowableValues;

@ApiModel(value = "InformVo", description = "消息通知传输对象")
@Getter
@Setter
@ToString
public class InformVo {

	/**
	 * 消息通知ID编号
	 */
	@ApiModelProperty(name = "id", dataType = "String", value = "消息通知ID编号")
	private String id;
	/**
	 * 消息通知通知对象ID
	 */
	@ApiModelProperty(name = "userId", dataType = "String", value = "消息通知对象ID")
	private String userId;
	/**
	 * 消息通知标题
	 */
	@ApiModelProperty(name = "title", required = true, dataType = "String", value = "消息通知标题")
	@NotBlank(message = "消息通知标题必填")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String title;
	/**
	 * 消息通知类型：（1：信息通知、2：发起审批、3：审批通过、4：审批拒绝）
	 */
	@ApiModelProperty(name = "type", required = true, dataType = "String", value = "消息通知类型：（1：信息通知、2：发起审批、3：审批通过、4：审批拒绝）", allowableValues = "1,2,3,4")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	@AllowableValues(allows = "1,2,3,4",message = "消息通知类型错误")  
	private String type;
	/**
	 * 消息通知内容
	 */
	@ApiModelProperty(name = "detail", required = true, dataType = "String", value = "消息通知内容")
	@NotBlank(message = "消息通知内容必填")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String detail;
	/**
	 * 通知信息关联数据载体,JOSN格式的数据
	 */
	@ApiModelProperty(name = "payload", dataType = "String", value = "通知信息关联数据载体,JOSN格式的数据")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String payload;
	/**
	 * 消息通知阅读状态：（0:未阅读、1:已阅读）
	 */
	@ApiModelProperty(name = "status", dataType = "String", value = "消息通知阅读状态：（0:未阅读、1:已阅读）", allowableValues = "0,1")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String status;
	/**
	 * 消息通知送达时间
	 */
	@ApiModelProperty(name = "time24", dataType = "String", value = "消息通知送达时间")
	private String time24;

}

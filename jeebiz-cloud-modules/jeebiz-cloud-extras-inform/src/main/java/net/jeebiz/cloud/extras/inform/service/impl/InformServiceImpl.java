/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.extras.inform.dao.IInformDao;
import net.jeebiz.cloud.extras.inform.dao.entities.InformModel;
import net.jeebiz.cloud.extras.inform.service.IInformService;
import net.jeebiz.cloud.extras.inform.web.vo.InformStatsVo;

@Service
public class InformServiceImpl extends BaseServiceImpl<InformModel, IInformDao> implements IInformService {

	@Override
	public InformStatsVo getStats(String userId) {
		
		Map<String, String> stats = getDao().getStats(userId);
		InformStatsVo statsVo = new InformStatsVo();
		statsVo.setCount(stats.get("count"));
		statsVo.setRead(stats.get("read"));
		statsVo.setUnread(stats.get("unread"));
		
		return statsVo;
	}

	@Override
	public int delInforms(String userId, List<String> ids) {
		return getDao().delInforms(userId, ids);
	}
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.web.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value = "InformStatsVo", description = "消息通知统计传输对象")
@Getter
@Setter
@ToString
public class InformStatsVo {

	/**
	 * 全部通知信息总数
	 */
	@ApiModelProperty(name = "count", dataType = "String", value = "全部通知信息总数")
	private String count;
	/**
	 * 已读消息数量
	 */
	@ApiModelProperty(name = "read", dataType = "String", value = "已读消息数量")
	private String read;
	/**
	 * 未读消息数量
	 */
	@ApiModelProperty(name = "unread", dataType = "String", value = "未读消息数量")
	private String unread;

}

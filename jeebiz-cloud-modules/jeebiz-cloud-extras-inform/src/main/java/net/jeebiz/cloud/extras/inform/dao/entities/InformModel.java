/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.dao.entities;

import java.util.List;

import org.apache.ibatis.type.Alias;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.dao.entities.PaginationModel;

@SuppressWarnings("serial")
@Alias(value = "InformModel")
@Getter
@Setter
@ToString
public class InformModel extends PaginationModel<InformModel> {

	/**
	 * 消息通知ID编号
	 */
	private String id;
	/**
	 * 消息通知ID
	 */
	private List<String> ids;
	/**
	 * 消息通知对象ID
	 */
	private String userId;
	/**
	 * 消息通知类型：（notice：通知、direct：私信）
	 */
	private String type;
	/**
	 * 通知信息标题
	 */
	private String title;
	/**
	 * 通知信息内容
	 */
	private String detail;
	/**
	 * 通知信息关联数据载体,JOSN格式的数据
	 */
	private String payload;
	/**
	 * 消息通知阅读状态：（0:未阅读、1:已阅读）
	 */
	private String status;
	/**
	 * 通知信息送达时间
	 */
	private String time24;

}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.service;

import java.util.List;

import net.jeebiz.cloud.api.service.IBaseService;
import net.jeebiz.cloud.extras.inform.dao.entities.InformModel;
import net.jeebiz.cloud.extras.inform.web.vo.InformStatsVo;

public interface IInformService extends IBaseService<InformModel> {
	
	/**
	 * 消息通知统计信息
	 * @return  {
	 *   count 		: 全部通知信息总数,
	 * 	 read 		: 已读通知信息总数,
	 * 	 unread 	: 未读通知信息总数,
	 * }
	 */
	InformStatsVo getStats(String userId);
	
	/**
	 * 删除用户的通知信息
	 * @param userId
	 * @param ids
	 * @return
	 */
	int delInforms(String userId,List<String> ids);
	
}

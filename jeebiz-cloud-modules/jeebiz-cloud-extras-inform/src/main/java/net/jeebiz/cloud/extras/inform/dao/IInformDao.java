/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.dao;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.jeebiz.cloud.extras.inform.dao.entities.InformModel;
import net.jeebiz.cloud.api.dao.BaseDao;

@Mapper
public interface IInformDao extends BaseDao<InformModel> {

	/**
	 * 消息通知统计信息
	 * @return  {
	 *   count 		: 全部通知信息总数,
	 * 	 read 		: 已读通知信息总数,
	 * 	 unread 	: 未读通知信息总数,
	 * }
	 */
	Map<String, String> getStats(@Param("userId") String userId);
	
	/**
	 * 删除用户的通知信息
	 * @param userid
	 * @param ids
	 * @return
	 */
	int delInforms(@Param("userId") String userId,@Param("ids") List<String> ids);
	
}

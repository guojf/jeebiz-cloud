/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.extras.inform.web.vo;

import org.hibernate.validator.constraints.SafeHtml;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.annotation.AllowableValues;
import net.jeebiz.cloud.api.vo.AbstractPaginationVo;

@ApiModel(value = "InformPaginationVo", description = "消息通知分页查询参数Vo")
@Getter
@Setter
@ToString
public class InformPaginationVo extends AbstractPaginationVo {

	/**
	 * 消息通知阅读状态：（0:未阅读、1:已阅读）
	 */
	@ApiModelProperty(name = "status", dataType = "String", value = "消息通知阅读状态：（0:未阅读、1:已阅读）", allowableValues = "0,1")
	@AllowableValues(allows = "0,1", message = "阅读状态：（0:未阅读、1:已阅读）", nullable = true)
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String status;

	/**
	 * 消息通知类型
	 */
	@ApiModelProperty(name = "type", dataType = "String", value = "消息通知类型")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String type;
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.service;

import net.jeebiz.cloud.api.service.IBaseService;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzPostModel;

public interface IAuthzPostService extends IBaseService<AuthzPostModel> {
	
}

/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.web.vo;

import org.hibernate.validator.constraints.SafeHtml;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.jeebiz.cloud.api.vo.AbstractPaginationVo;

@ApiModel(value = "AuthzTeamPaginationVo", description = "团队信息分页查询参数")
@Getter
@Setter
@ToString
public class AuthzTeamPaginationVo extends AbstractPaginationVo {

	/**
	 * 机构ID编号
	 */
	@ApiModelProperty(name = "orgId", dataType = "String", value = "机构ID编号")
	private String orgId;
	/**
	 * 部门ID编号
	 */
	@ApiModelProperty(name = "deptId", dataType = "String", value = "部门ID编号")
	private String deptId;
	/**
	 * 团队名称
	 */
	@ApiModelProperty(name = "name", dataType = "String", value = "团队名称")
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	private String name;

}

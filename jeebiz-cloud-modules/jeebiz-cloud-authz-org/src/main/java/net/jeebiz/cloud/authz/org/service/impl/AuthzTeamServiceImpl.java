/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.org.dao.IAuthzTeamDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzTeamModel;
import net.jeebiz.cloud.authz.org.service.IAuthzTeamService;

@Service
public class AuthzTeamServiceImpl extends BaseServiceImpl<AuthzTeamModel, IAuthzTeamDao> implements IAuthzTeamService{

	@Override
	public int getTeamCountByName(String name, String deptId, String teamId) {
		return getDao().getTeamCountByName(name, deptId, teamId);
	}

	@Override
	public int getStaffCount(String id) {
		return getDao().getStaffCount(id);
	}
	
}
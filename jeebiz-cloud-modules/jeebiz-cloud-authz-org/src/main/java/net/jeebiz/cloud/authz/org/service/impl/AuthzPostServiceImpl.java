/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.org.dao.IAuthzPostDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzPostModel;
import net.jeebiz.cloud.authz.org.service.IAuthzPostService;

@Service
public class AuthzPostServiceImpl extends BaseServiceImpl<AuthzPostModel, IAuthzPostDao> implements IAuthzPostService{

}
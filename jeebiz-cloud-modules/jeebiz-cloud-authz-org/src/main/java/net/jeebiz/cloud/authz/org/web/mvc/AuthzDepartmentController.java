package net.jeebiz.cloud.authz.org.web.mvc;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.boot.biz.userdetails.SecurityPrincipal;
import org.springframework.security.boot.utils.SubjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.jeebiz.cloud.api.ApiRestResponse;
import net.jeebiz.cloud.api.annotation.BusinessLog;
import net.jeebiz.cloud.api.annotation.BusinessType;
import net.jeebiz.cloud.api.dao.entities.PairModel;
import net.jeebiz.cloud.api.utils.CollectionUtils;
import net.jeebiz.cloud.api.utils.HttpStatus;
import net.jeebiz.cloud.api.webmvc.BaseApiController;
import net.jeebiz.cloud.api.webmvc.Result;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzDepartmentModel;
import net.jeebiz.cloud.authz.org.service.IAuthzDepartmentService;
import net.jeebiz.cloud.authz.org.setup.Constants;
import net.jeebiz.cloud.authz.org.web.vo.AuthzDepartmentNewVo;
import net.jeebiz.cloud.authz.org.web.vo.AuthzDepartmentPaginationVo;
import net.jeebiz.cloud.authz.org.web.vo.AuthzDepartmentRenewVo;
import net.jeebiz.cloud.authz.org.web.vo.AuthzDepartmentVo;

@Api(tags = "组织机构：部门信息维护")
@RestController
@RequestMapping(value = "/authz/org/dept/")
public class AuthzDepartmentController extends BaseApiController {

	@Autowired
	private IAuthzDepartmentService authzDepartmentService;

	@ApiOperation(value = "分页查询部门信息", notes = "分页查询部门信息")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "body", name = "paginationVo", value = "分页查询参数", dataType = "AuthzDepartmentPaginationVo") 
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "分页查询部门信息", opt = BusinessType.SELECT)
	@PostMapping("list")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:list') or hasAuthority('*')) ")
	@ResponseBody
	public Object list(@Valid @RequestBody AuthzDepartmentPaginationVo paginationVo) throws Exception {
		
		AuthzDepartmentModel model = getBeanMapper().map(paginationVo, AuthzDepartmentModel.class);
		
		Page<AuthzDepartmentModel> pageResult = getAuthzDepartmentService().getPagedList(model);
		List<AuthzDepartmentVo> retList = Lists.newArrayList();
		for (AuthzDepartmentModel departmentModel : pageResult.getRecords()) {
			retList.add(getBeanMapper().map(departmentModel, AuthzDepartmentVo.class));
		}
		
		return new Result<AuthzDepartmentVo>(pageResult, retList);
		
	}
	
	@ApiOperation(value = "部门信息：数据列表集合", notes = "根据机构ID查询部门信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "orgId", value = "机构ID编码", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "查询部门信息", opt = BusinessType.SELECT)
	@GetMapping("list")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:list') or hasAuthority('*')) ")
	@ResponseBody
	public Object list(@RequestParam(required = false) String orgId) throws Exception {
		
		List<AuthzDepartmentModel> resultList = getAuthzDepartmentService().getModelList(orgId);
		if( CollectionUtils.isEmpty(resultList)) {
			return ApiRestResponse.empty(getMessage("authz.dept.not-found"));
		}
		List<AuthzDepartmentVo> retList = Lists.newArrayList();
		for (AuthzDepartmentModel model : resultList) {
			retList.add(getBeanMapper().map(model, AuthzDepartmentVo.class));
		}
		return ApiRestResponse.success(retList);
	}
	
	@ApiOperation(value = "部门信息：键值对集合", notes = "根据机构ID查询部门信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "orgId", value = "机构ID编码", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "查询部门信息", opt = BusinessType.SELECT)
	@GetMapping("pairs")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:list') or hasAuthority('*')) ")
	@ResponseBody
	public Object pairs(@RequestParam(required = false) String orgId) throws Exception {
		return ApiRestResponse.success(getAuthzDepartmentService().getPairValues(orgId));
	}
	
	@ApiOperation(value = "创建部门信息", notes = "增加一个新的部门信息")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "body", name = "deptVo", value = "部门信息", required = true, dataType = "AuthzDepartmentNewVo") 
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "创建部门信息", opt = BusinessType.INSERT)
	@PostMapping("new")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:new') or hasAuthority('*')) ")
	@ResponseBody
	public Object newDept(@Valid @RequestBody AuthzDepartmentNewVo deptVo) throws Exception {
		
		int count1 = getAuthzDepartmentService().getCountByCode(deptVo.getCode(), deptVo.getOrgId(), null);
		if(count1 > 0) {
			return fail("authz.dept.new.code-exists");
		}
		int count2 = getAuthzDepartmentService().getCountByName(deptVo.getName(), deptVo.getOrgId(), null);
		if(count2 > 0) {
			return fail("authz.dept.new.name-exists");
		}
		
		AuthzDepartmentModel model = getBeanMapper().map(deptVo, AuthzDepartmentModel.class);
		SecurityPrincipal principal = SubjectUtils.getPrincipal(SecurityPrincipal.class);
		model.setUserId(principal.getUserid());
		// 新增一条数据库配置记录
		int result = getAuthzDepartmentService().insert(model);
		if(result > 0) {
			return success("authz.dept.new.success", result);
		}
		return fail("authz.dept.new.fail", result);
	}
	
	@ApiOperation(value = "更新部门信息", notes = "更新部门信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "body", name = "deptVo", value = "部门信息", required = true, dataType = "AuthzDepartmentRenewVo") 
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "更新部门信息", opt = BusinessType.UPDATE)
	@PostMapping("renew")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:renew') or hasAuthority('*')) ")
	@ResponseBody
	public Object renew(@Valid @RequestBody AuthzDepartmentRenewVo deptVo) throws Exception {
		
		int count1 = getAuthzDepartmentService().getCountByCode(deptVo.getCode(), deptVo.getOrgId(), deptVo.getId());
		if(count1 > 0) {
			return fail("authz.dept.renew.code-exists");
		}
		int count2 = getAuthzDepartmentService().getCountByName(deptVo.getName(), deptVo.getOrgId(), deptVo.getId());
		if(count2 > 0) {
			return fail("authz.dept.renew.name-exists");
		}
		
		AuthzDepartmentModel model = getBeanMapper().map(deptVo, AuthzDepartmentModel.class);
		int result = getAuthzDepartmentService().update(model);
		if(result == 1) {
			return success("authz.dept.renew.success", result);
		}
		// 逻辑代码，如果发生异常将不会被执行
		return fail("authz.dept.renew.fail", result);
	}
	
	@ApiOperation(value = "更新部门信息状态", notes = "更新部门信息状态")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", required = true, value = "部门信息ID", dataType = "String"),
		@ApiImplicitParam(name = "status", required = true, value = "部门信息状态", dataType = "String", allowableValues = "1,0")
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "更新部门信息状态", opt = BusinessType.UPDATE)
	@PostMapping("status")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:status') or hasAuthority('*')) ")
	@ResponseBody
	public Object status(@RequestParam String id, @RequestParam String status) throws Exception {
		int result = getAuthzDepartmentService().setStatus(id, status);
		if(result == 1) {
			return success("authz.dept.status.success", result);
		}
		// 逻辑代码，如果发生异常将不会被执行
		return fail("authz.dept.status.fail", result);
	}
	
	@ApiOperation(value = "删除部门信息", notes = "删除部门信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "path", name = "id", value = "部门信息ID", required = true, dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "删除部门信息", opt = BusinessType.UPDATE)
	@GetMapping("delete/{id}")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:delete') or hasAuthority('*')) ")
	@ResponseBody
	public Object delete(@PathVariable("id") String id) throws Exception {
		
		int count1 = getAuthzDepartmentService().getCountByParent(id);
		if(count1 > 0 ) {
			return fail("authz.dept.delete.child-exists");
		}
		int count2 = getAuthzDepartmentService().getStaffCount(id);
		if(count2 > 0 ) {
			return fail("authz.dept.delete.staff-exists");
		}
		
		int result = getAuthzDepartmentService().delete(id);
		if(result > 0) {
			return success("authz.dept.delete.success", result);
		}
		// 逻辑代码，如果发生异常将不会被执行
		return fail("authz.dept.delete.fail", result);
	}
	
	@ApiOperation(value = "查询部门信息", notes = "根据ID查询部门信息")
	@ApiImplicitParams({ 
		@ApiImplicitParam(paramType = "path", name = "id", required = true, value = "部门信息ID", dataType = "String")
	})
	@BusinessLog(module = Constants.AUTHZ_DEPT, business = "查询部门信息", opt = BusinessType.SELECT)
	@GetMapping("detail/{id}")
	@PreAuthorize("authenticated and (hasAuthority('authz-dept:detail') or hasAuthority('*')) ")
	@ResponseBody
	public Object detail(@PathVariable("id") String id) throws Exception { 
		AuthzDepartmentModel model = getAuthzDepartmentService().getModel(id);
		if( model == null) {
			return ApiRestResponse.empty(getMessage("authz.dept.not-found"));
		}
		return getBeanMapper().map(model, AuthzDepartmentVo.class);
	}

	public IAuthzDepartmentService getAuthzDepartmentService() {
		return authzDepartmentService;
	}
	
}

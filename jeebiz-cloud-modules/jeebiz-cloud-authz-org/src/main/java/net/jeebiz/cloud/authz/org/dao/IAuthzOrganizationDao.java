/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.jeebiz.cloud.api.dao.BaseDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzOrganizationModel;

@Mapper
public interface IAuthzOrganizationDao extends BaseDao<AuthzOrganizationModel> {

	public int getRootCount();
	
	public int getDeptCount(@Param("id")String id);
}

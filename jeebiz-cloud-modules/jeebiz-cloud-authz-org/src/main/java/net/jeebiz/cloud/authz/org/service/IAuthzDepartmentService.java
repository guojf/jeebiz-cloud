/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.service;

import net.jeebiz.cloud.api.service.IBaseService;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzDepartmentModel;

public interface IAuthzDepartmentService extends IBaseService<AuthzDepartmentModel> {
	
	/**
	 * 根据编码获取记录数
	 * @return
	 */
	public int getCountByCode(String code, String orgId, String deptId);
	
	/**
	 * 根据名称获取记录数
	 * @return
	 */
	public int getCountByName(String name, String orgId, String deptId);
	
	/**
	 * 获取部门下成员梳理
	 * @param id
	 * @return
	 */
	public int getStaffCount(String id);
	
}

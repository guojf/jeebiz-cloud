/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.dao;

import org.apache.ibatis.annotations.Mapper;

import net.jeebiz.cloud.api.dao.BaseDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzPostModel;

@Mapper
public interface IAuthzPostDao extends BaseDao<AuthzPostModel> {

}

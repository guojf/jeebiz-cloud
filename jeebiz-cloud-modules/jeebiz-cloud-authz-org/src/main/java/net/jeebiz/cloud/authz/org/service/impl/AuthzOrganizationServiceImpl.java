/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.org.dao.IAuthzOrganizationDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzOrganizationModel;
import net.jeebiz.cloud.authz.org.service.IAuthzOrganizationService;

@Service
public class AuthzOrganizationServiceImpl extends BaseServiceImpl<AuthzOrganizationModel, IAuthzOrganizationDao> implements IAuthzOrganizationService{

	@Override
	public int getRootCount() {
		return getDao().getRootCount();
	}
	
	@Override
	public int getDeptCount(String id) {
		return getDao().getDeptCount(id);
	}
	
}

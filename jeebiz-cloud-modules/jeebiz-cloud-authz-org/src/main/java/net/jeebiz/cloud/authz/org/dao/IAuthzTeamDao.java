package net.jeebiz.cloud.authz.org.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.jeebiz.cloud.api.dao.BaseDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzTeamModel;

@Mapper
public interface IAuthzTeamDao extends BaseDao<AuthzTeamModel> {

	/**
	 * 根据名称获取记录数
	 * @return
	 */
	public int getTeamCountByName(@Param("name") String name, @Param("deptId") String deptId, @Param("teamId") String teamId);
	
	/**
	 * 获取小组下成员梳理
	 * @param id
	 * @return
	 */
	public int getStaffCount(@Param("id") String id);
	
}

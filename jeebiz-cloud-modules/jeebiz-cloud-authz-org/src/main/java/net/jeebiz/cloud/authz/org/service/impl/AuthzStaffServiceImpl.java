/** 
 * Copyright (C) 2018 Jeebiz (http://jeebiz.net).
 * All Rights Reserved. 
 */
package net.jeebiz.cloud.authz.org.service.impl;

import org.springframework.stereotype.Service;

import net.jeebiz.cloud.api.service.BaseServiceImpl;
import net.jeebiz.cloud.authz.org.dao.IAuthzStaffDao;
import net.jeebiz.cloud.authz.org.dao.entities.AuthzStaffModel;
import net.jeebiz.cloud.authz.org.service.IAuthzStaffService;

@Service
public class AuthzStaffServiceImpl extends BaseServiceImpl<AuthzStaffModel, IAuthzStaffDao> implements IAuthzStaffService{

}

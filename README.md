## Jeebiz Cloud 简介：

> Jeebiz Cloud 是基于Spring Cloud 项目整合已有的技术组件，构建的企业级业务场景需要的微服务快速开发脚手架；

- 1、继承Spring Cloud具有的各种特性；

- 2、整合主流技术：Mybatis-Plus、J2cache、Redis、Spring Security、Swagger
- 3、自带通用实现：认证服务、日志服务、消息服务、基础数据、文件存储服务等；
- 4、项目快速搭建：基于已有的结构和默认的配置，可快速的搭建一个独立微服务；
- 5、脚本自动执行：项目整合了Flyway脚本管理，每次的数据库变动都由脚本完成，并且具备脚本版本管理功能，基于脚本的全自动升级；从技术上解决之前项目启动前必须先初始化数据库 和 不断的增量升级过程中可能出现的数据库异常；
- 6、Docker镜像支持：全局整理了Docker镜像构建环境，仅需要配置好Maven插件、本机Docker环境、Docker私有厂库 即可实现编译打包期间自动构建Docker镜像，借助开源软件 Harbor、Kubernetes、Rancher 可实现基于Docker的运维支撑；

## 其他资料：

Spring Cloud 指导手册：
https://springcloud.cc/spring-cloud-dalston.html

Spring Cloud 优秀博文推荐：
http://blog.didispace.com/Spring-Cloud%E5%9F%BA%E7%A1%80%E6%95%99%E7%A8%8B/

Spring Cloud 相关项目
https://springcloud.cc/